# COMPUTRITION MODULE

## CONTENTS

 - [Introduction](#introduction)
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Configuration](#configuration)
 - [Submodules](#submodules)
   - [Computrition Entity](#computrition-entity)
 - [Maintainers](#maintainers)
 - [Sponsors](#sponsors)

## INTRODUCTION

The Computrition module enables your Drupal website to quickly and easily fetch
data from your instance of the [Computrition Hospitality
Suite](https://www.computrition.com/product-solutions/). In addition to
providing functions to allow you to fetch and process the data in your own code,
by installing the bundled "Computrition Entity" submodule you can also
automatically import all Computrition data into your drupal site as custom
entities (either via cron or drush commands).

For a full description of the module, visit the project page:
https://www.drupal.org/project/computrition

To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/computrition

The maintainers of this module are not affiliated with Computrition.

## REQUIREMENTS

This module requires a working and configured *Computrition HSWS XChange
Gateway*, version 19.4 or higher. Neither this module nor its submodules require
any modules outside of Drupal core.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION

The configuration page is located in the menu at *Administration » Configuration
 » Web services » Computrition* (`/admin/config/services/computrition`).

At a minimum, you will be required to enter the URL of the *Computrition
Hospitality Suite Web Service XML API* for the module to function.

## SUBMODULES

### Computrition Entity (computrition_entity)

By enabling this optional submodule, a new "Computrition object" entity type
will be created. The module can then be configured to automatically import and
synchronize all data in your linked Computrition instance as entities in your
drupal site.

If you prefer to do the imports via drush instead of drupal cron, the following
commands are available to you:
 - `computrition_entity:import-all (ceia)`: Run a full import of all
   computrition objects into drupal entities.
 - `computrition_entity:purge-orphan-objects (cepoo)`: Delete any entities that
   have been deleted on the Computrition end.
 - `computrition_entity:delete-invalid (cedi)`: Delete invalid Computrition
   object entities from the Drupal site.

Additionally, the module also provides hooks to further alter any imported data
objects from computrition:
 - `hook_computrition_location_import_alter(ComputritionObject &$object)`
 - `hook_computrition_meal_import_alter(ComputritionObject &$object)`
 - `hook_computrition_menu_type_import_alter(ComputritionObject &$object)`
 - `hook_computrition_nutrient_import_alter(ComputritionObject &$object)`
 - `hook_computrition_publishing_group_import_alter(ComputritionObject
   &$object)`
 - `hook_computrition_recipe_category_import_alter(ComputritionObject &$object)`
 - `hook_computrition_recipe_source_import_alter(ComputritionObject &$object)`
 - `hook_computrition_recipe_import_alter(ComputritionObject &$object)`
 - `hook_computrition_menu_import_alter(ComputritionObject &$object)`

## MAINTAINERS

 - [Fernando Iglesias](https://www.drupal.org/u/fernando-iglesias) - Creator,
 Maintainer
 - [Jeffrey Mattson](https://www.drupal.org/u/jeffreysmattson) - Maintainer
 - [Benjamin Morgan](https://www.drupal.org/u/t00lie) - Maintainer

## SPONSORS

This project has been sponsored by:
 - **Dartmouth College**

   One of the world's greatest academic institutions and a member of the Ivy
   League, Dartmouth has been educating leaders since 1769. Visit
   https://home.dartmouth.edu for more information.
