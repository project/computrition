<?php

namespace Drupal\computrition\Api;

/**
 * Defines the various computrition error response codes from the external API.
 */
class ComputritionErrorResponse {

  /**
   * Computrition error code -20680.
   *
   * Cannot retrieve menu information for the passed date/menu combination.
   */
  const INVALID_MENU_DATE_COMBINATION = '-20680';

  /**
   * Computrition error code -20681.
   *
   * Cannot connect to schema. Administrative lockout.
   */
  const ADMINISTRATIVE_LOCKOUT = '-20681';

  /**
   * Computrition error code -20682.
   *
   * Cannot process selections. Invalid hash key.
   */
  const INVALID_HASH_KEY = '-20682';

  /**
   * Computrition error code -20685.
   *
   * Cannot process selections because menu lock was removed by another user.
   */
  const MENU_LOCK_REMOVED = '-20685';

  /**
   * Computrition error code -20687.
   *
   * No guest menu assignment found.
   */
  const NO_GUEST_MENU_ASSIGNMENT = '-20687';

  /**
   * Computrition error code -20688.
   *
   * Cannot process selections. Menu ID does not match existing menus.
   */
  const INVALID_MENU_ID = '-20688';

  /**
   * Computrition error code -20689.
   *
   * Cannot find requested item.
   */
  const NOT_FOUND = '-20689';

  /**
   * Computrition error code -20690.
   *
   * Post Meal Counts already set for this menu.
   */
  const POST_MEAL_COUNTS_ALREADY_SET = '-20690';

  /**
   * Computrition error code -20691.
   *
   * Cannot find menu for passed parameters.
   */
  const INVALID_MENU_PARAMETERS = '-20691';

  /**
   * Computrition error code -20692.
   *
   * Cannot find recipe information because the master reference value belongs
   * to more than one recipe.
   */
  const MASTER_REFERENCE_BELONGS_TO_MULTIPLE_RECIPES = '-20692';

  /**
   * Computrition error code -20693.
   *
   * Cannot return item information because the master reference value belongs
   * to more than one item.
   */
  const MASTER_REFERENCE_BELONGS_TO_MULTIPLE_ITEMS = '-20693';

  /**
   * Computrition error code -20694.
   *
   * Cannot process production menu. Invalid portion size values for recipe ID
   * xxxxxxxxxx -numerator xxxxxxxxxx, denominator xxxxxxxxxx.
   */
  const INVALID_PORTION_SIZE = '-20694';

  /**
   * Computrition error code -20695.
   *
   * Cannot process production menu. Invalid delivery time. Delivery time for
   * this meal must be between xx:xx and xx:xx.
   */
  const INVALID_DELIVERY_TIME = '-20695';

}
