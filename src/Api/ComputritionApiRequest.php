<?php

namespace Drupal\computrition\Api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Makes a request to the external Computrition API.
 */
class ComputritionApiRequest {

  /**
   * The base URL of the external Computrition API that we're making calls to.
   *
   * @var string
   */
  protected $apiBaseUrl;

  /**
   * The cache lifetime (in minutes) to store Computrition API responses.
   *
   * @var string
   */
  protected $cacheLifetimeMinutes;

  /**
   * Whether failed Computrition requests should be written to the database log.
   *
   * @var string
   */
  protected $logBadResponses;

  /**
   * The CacheBackendInterface used to make calls to the Drupal cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Guzzle client used to make calls to the external Computrition API.
   *
   * @var \GuzzleHttp\Client
   */
  protected $guzzleClient;

  /**
   * Logger factory used for writing to the Drupal database log.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * An array of returned Computrition response codes and their severity level.
   *
   * @var array
   */
  protected $responseCodesSeverity = [
    ComputritionErrorResponse::INVALID_MENU_DATE_COMBINATION => RfcLogLevel::NOTICE,
    ComputritionErrorResponse::ADMINISTRATIVE_LOCKOUT => RfcLogLevel::WARNING,
    ComputritionErrorResponse::INVALID_HASH_KEY => RfcLogLevel::WARNING,
    ComputritionErrorResponse::MENU_LOCK_REMOVED => RfcLogLevel::WARNING,
    ComputritionErrorResponse::NO_GUEST_MENU_ASSIGNMENT => RfcLogLevel::NOTICE,
    ComputritionErrorResponse::INVALID_MENU_ID => RfcLogLevel::WARNING,
    ComputritionErrorResponse::NOT_FOUND => RfcLogLevel::NOTICE,
    ComputritionErrorResponse::POST_MEAL_COUNTS_ALREADY_SET => RfcLogLevel::WARNING,
    ComputritionErrorResponse::INVALID_MENU_PARAMETERS => RfcLogLevel::NOTICE,
    ComputritionErrorResponse::MASTER_REFERENCE_BELONGS_TO_MULTIPLE_RECIPES => RfcLogLevel::WARNING,
    ComputritionErrorResponse::MASTER_REFERENCE_BELONGS_TO_MULTIPLE_ITEMS => RfcLogLevel::WARNING,
    ComputritionErrorResponse::INVALID_PORTION_SIZE => RfcLogLevel::WARNING,
    ComputritionErrorResponse::INVALID_DELIVERY_TIME => RfcLogLevel::WARNING,
  ];

  /**
   * ComputritionApiRequest constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The computrition module settings, used for setting the API base url and
   *   cache lifetime.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The CacheBackendInterface used to make calls to the Drupal cache.
   * @param \GuzzleHttp\Client $guzzle_client
   *   The guzzle client used to make calls to the external computrition API.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger factory used for writing messages to the Drupal database log.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend, Client $guzzle_client, LoggerChannelFactory $logger) {
    $config = $config_factory->get('computrition.settings');
    $this->apiBaseUrl = $config->get('api_base_url');
    $this->cacheLifetimeMinutes = $config->get('cache_lifetime_minutes');
    $this->logBadResponses = $config->get('log_bad_responses');
    $this->cacheBackend = $cache_backend;
    $this->guzzleClient = $guzzle_client;
    $this->logger = $logger->get('computrition');
  }

  /**
   * Get the currently configured computrition HSWS endpoint URL.
   *
   * @return string
   *   The base URL of the external Computrition API.
   */
  public function getApiBaseUrl() {
    return $this->apiBaseUrl;
  }

  /**
   * Get the currently configured API response cache lifetime (in minutes).
   *
   * @return int
   *   The cache lifetime (in minutes) to store Computrition API responses.
   */
  public function getCacheLifetimeMinutes() {
    return $this->cacheLifetimeMinutes;
  }

  /**
   * Fetch a list of locations from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of locations.
   */
  public function getLocationList(array $params = []) {
    $method = 'GET';
    $endpoint = 'location/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      $items = $data['HSWS']['LOCATIONS']['LOCATION'] ?? [];
      if (isset($data['HSWS']['LOCATIONS']['LOCATION'][0])) {
        $items = $data['HSWS']['LOCATIONS']['LOCATION'] ?? [];
      }
      else {
        $items = [$data['HSWS']['LOCATIONS']['LOCATION'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all meals from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of meals.
   */
  public function getMealList(array $params = []) {
    $method = 'GET';
    $endpoint = 'meal/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['MEALS']['MEAL'][0])) {
        $items = $data['MEALS']['MEAL'] ?? [];
      }
      else {
        $items = [$data['MEALS']['MEAL'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all menu types from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of all menu types.
   */
  public function getMenuTypeList(array $params = []) {
    $method = 'GET';
    $endpoint = 'menu_type/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['MENUS']['MENU'][0])) {
        $items = $data['MENUS']['MENU'] ?? [];
      }
      else {
        $items = [$data['MENUS']['MENU'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all nutrients from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of all nutrients.
   */
  public function getNutrientList(array $params = []) {
    $method = 'GET';
    $endpoint = 'nutrient/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['NUTRIENTS']['NUTRIENT'][0])) {
        $items = $data['NUTRIENTS']['NUTRIENT'] ?? [];
      }
      else {
        $items = [$data['NUTRIENTS']['NUTRIENT'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all publishing groups from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of all publishing groups.
   */
  public function getPublishingGroupList(array $params = []) {
    $method = 'GET';
    $endpoint = 'publishing_group/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['PUBLISHING_GROUPS']['GROUP'][0])) {
        $items = $data['PUBLISHING_GROUPS']['GROUP'] ?? [];
      }
      else {
        $items = [$data['PUBLISHING_GROUPS']['GROUP'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all recipe categories from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of all recipe categories.
   */
  public function getRecipeCategoryList(array $params = []) {
    $method = 'GET';
    $endpoint = 'recipe_category/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['CATEGORIES']['CATEGORY'][0])) {
        $items = $data['CATEGORIES']['CATEGORY'] ?? [];
      }
      else {
        $items = [$data['CATEGORIES']['CATEGORY'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all recipe sources from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to append to the request.
   *
   * @return array
   *   An array representing a list of all recipe sources.
   */
  public function getRecipeSourceList(array $params = []) {
    $method = 'GET';
    $endpoint = 'recipe_source/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['RECIPESOURCES']['SOURCE'][0])) {
        $items = $data['RECIPESOURCES']['SOURCE'] ?? [];
      }
      else {
        $items = [$data['RECIPESOURCES']['SOURCE'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of menus from Computrition.
   *
   * The response returns a list of all production menus as specified in the
   * HSWS.conf file for a specified date and sorted by meal and name.
   *
   * NOTE: This is undocumented behavior, but if you're receiving no results
   * from this function, you may need to pass in a 'sid' parameter matching a
   * location returned from getLocationList().
   *
   * @param string $date
   *   Display menus from this date in the response. It is required and must be
   *   in the format YYYYMMDD.
   * @param array $params
   *   An optional array of parameters to filter the results set. The following
   *   additional parameters are available:
   *     - columns (string) The specific optional attribute(s) to include in the
   *       response.
   *
   *     - meal (int) Filter the response based on the meal ID.
   *
   *     - menutype (int) Filter the response based on the menu type ID.
   *
   * @return array
   *   An array representing a list of menus.
   */
  public function getMenuList($date, array $params = []) {
    $method = 'GET';
    $endpoint = 'menu/list';

    $params = [
      'date' => $date,
    ] + $params;

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['PRODMENUS']['MENU'][0])) {
        $items = $data['PRODMENUS']['MENU'] ?? [];
      }
      else {
        $items = [$data['PRODMENUS']['MENU'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a list of all recipes from Computrition.
   *
   * @param array $params
   *   An optional array of parameters to filter the results set. The following
   *   additional parameters are available:
   *     - columns (string) The specific optional attribute(s) to include in the
   *       response.
   *
   *     - category (string) Filter the response for the specified category.
   *
   *     - isactive (string) Filter the response based on the isactive column.
   *       Possible values are t, f, or all. Default is t.
   *
   *     - isnourishment (string) Filter the response based on the istaxable
   *       column. Possible values are t, f, or all. Default is all.
   *
   *     - portionsize (string) Filter the response for the specified portion
   *       size. This parameter also gates portionnumerator and
   *       portiondenominator.
   *
   *     - publishinggroup (string) Filter the response for the specified
   *       publishing group.
   *
   *     - recipesource (string) Filter the response for the specified recipe
   *       source.
   *
   *     - nutrients (string) Filter the response for the specified nutrient(s).
   *       Possible values are none, def (default nutrients), all, or a list of
   *       specific nutrients separated with +. The list is in addition to the
   *       default nutrients defined in the Web Service configuration file.
   *       Default is none.
   *
   *     - roundingmethod (string) If nutrients are included in the response,
   *       you can set this to raw or fda (FDA-compliant). Default is raw.
   *
   *     - sort (string) Sort the response based on the specified attribute
   *       name(s). Default is sorted by name.
   *
   * @return array
   *   An array representing a list of recipes.
   */
  public function getRecipeList(array $params = []) {
    $method = 'GET';
    $endpoint = 'recipe/list';

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      if (isset($data['RECIPES']['RECIPE'][0])) {
        $items = $data['RECIPES']['RECIPE'] ?? [];
      }
      else {
        $items = [$data['RECIPES']['RECIPE'] ?? []];
      }

      foreach ($items as $item) {
        $output[] = self::formatArrayKeys($item);
      }
    }

    return $output;
  }

  /**
   * Fetch a specific menu from Computrition.
   *
   * The response returns menu and recipe information for the menu specified
   * with the id parameter. The recipe information includes allergen and
   * ingredient paragraphs that comply with current food labeling laws and the
   * Code of Federal Regulations. The ingredients list includes sub-ingredients
   * unless you specify to exclude them with the exclude_subingredients
   * parameter.
   *
   * NOTE: This is undocumented behavior, but if you're receiving no results
   * from this function, you may need to pass in a 'sid' parameter matching a
   * location returned from getLocationList().
   *
   * @param int $id
   *   The ID number(s) of the menu(s) to return in the response. This parameter
   *   is required.
   * @param array $params
   *   An optional array of parameters to filter the results set. The following
   *   additional parameters are available:
   *   - exclude_subingredients (string) Whether to exclude sub-ingredients from
   *     the ingredientstext column. Possible values are t or f. Default is f.
   *
   *   - nutrients (string) Filter the response for the specified nutrient(s).
   *     Possible values are none, def (default nutrients), all, or a list of
   *     specific nutrients separated with +. The list is in addition to the
   *     default nutrients defined in the Web Service configuration file.
   *     Default is none.
   *
   *   - roundingmethod (string) If nutrients are included in the response, you
   *     can set this to raw or fda (FDA-compliant). Default is raw.
   *
   * @return array
   *   An array representing a menu.
   */
  public function getMenu($id, array $params = []) {
    $method = 'GET';
    $endpoint = 'menu';

    $params = [
      'id' => $id,
    ] + $params;

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      $output['menu'] = $data['PRODMENUS']['MENU'] ?? [];
      $output['recipes'] = $data['PRODMENUS']['RECIPES']['RECIPE'] ?? [];

      // Strangely, the API results are formatted differently if there is only
      // a single item in the recipe list, we have to handle the special case
      // explicitly.
      if (!empty($output['recipes']) && isset($output['recipes']['@id'])) {
        $recipe = $output['recipes'];
        $output['recipes'] = [$recipe];
      }

      $nutrients = $data['PRODMENUS']['RECIPES']['NUTRIENTS'] ?? '';

      // Parse the nutrients of each recipe into a useful format.
      foreach ($output['recipes'] as $index => $recipe) {
        if (is_array($recipe)) {
          $output['recipes'][$index]['@nutrients'] = self::parseNutrients(
            $nutrients,
            $recipe['@nutrients'] ?? '',
            $recipe['@nutrientsuncertain'] ?? '',
            $recipe['@percentdailyvalue'] ?? ''
          );
          unset($output['recipes'][$index]['@nutrientsuncertain']);
          unset($output['recipes'][$index]['@percentdailyvalue']);
          $output['recipes'][$index] = self::formatArrayKeys($output['recipes'][$index]);
        }
      }

      $output['menu'] = self::formatArrayKeys($output['menu']);
    }

    return $output;
  }

  /**
   * Fetch a specific recipe from Computrition.
   *
   * The response returns the recipe specified using either id or
   * masterreference. If both parameters are included, the id value is used and
   * masterreference is ignored. The response also returns ingredient/method and
   * LDA information for the recipe if specified using parameters.
   *
   * @param int $id
   *   The ID number(s) of the recipe(s) to return in the response. This is only
   *   required if a masterreference is not provided in the optional $params
   *   array.
   * @param array $params
   *   An optional array of parameters to filter the results set. The following
   *   additional parameters are available:
   *   - masterreference (int) The master reference number of the recipe to
   *     return in the response.
   *
   *   - columns (string) Specify the optional attributes to include in the
   *     response.
   *
   *   - ingredients (string) Whether to include recipe ingredients in the
   *     response. Possible values are True and False. Default is False.
   *
   *   - ldas (string) Whether to include recipe LDAs in the response. Possible
   *     values are True and False. Default is False.
   *
   *   - methods (string) Whether to include the recipe method in the response.
   *     Only possible value to include the method is text, otherwise method is
   *     excluded.
   *
   *   - nutrients (string) Filter the response for the specified nutrient(s).
   *     Possible values are none, def (default nutrients), all, or a list of
   *     specific nutrients separated with +. The list is in addition to the
   *     default nutrients defined in the Web Service configuration file.
   *     Default is none.
   *
   *   - roundingmethod (string) If nutrients are included in the response, you
   *     can set this to raw or fda (FDA-compliant). Default is raw.
   *
   * @return array
   *   An array representing a recipe.
   */
  public function getRecipe($id = NULL, array $params = []) {
    $method = 'GET';
    $endpoint = 'recipe';

    if ($id) {
      $params = [
        'id' => $id,
      ] + $params;
    }

    $output = [];
    if ($data = $this->execute($method, $endpoint, $params)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      $recipe = $data['RECIPES']['RECIPE'] ?? [];

      // Parse the nutrients into a useful format.
      $recipe['@nutrients'] = self::parseNutrients(
        $data['RECIPES']['NUTRIENTS'] ?? '',
        $recipe['@nutrients'] ?? '',
        $recipe['@nutrientsuncertain'] ?? '',
        $recipe['@percentdailyvalue'] ?? ''
      );
      unset($recipe['@nutrientsuncertain']);
      unset($recipe['@percentdailyvalue']);

      // If present, parse the ingredients.
      $ingredients = $data['RECIPES']['INGREDIENTS']['INGREDIENT'] ?? [];
      if (!empty($ingredients)) {
        foreach ($ingredients as $ingredient) {
          $recipe['@ingredients'][] = self::formatArrayKeys($ingredient);
        }
      }

      // If present, parse the methods.
      $methods = $data['RECIPES']['METHODS']['METHOD'] ?? [];
      if (!empty($methods)) {
        foreach ($methods as $method) {
          $recipe['@methods'][] = self::formatArrayKeys($method);
        }
      }

      // If present, parse the LDAs.
      $ldas = $data['RECIPES']['LDAS']['LDA'] ?? [];
      if (!empty($ldas)) {
        foreach ($ldas as $lda) {
          $recipe['@ldas'][] = self::formatArrayKeys($lda);
        }
      }

      $output = self::formatArrayKeys($recipe);
    }

    return $output;
  }

  /**
   * Fetch a total nutrient summary from Computrition.
   *
   * This request is an HTML POST call that retrieves nutrient data for a set of
   * recipes selected from a menu. In addition, it returns nutrients per the
   * individual recipe selection and nutrient total values and % Daily Values
   * for the set of recipe selections.
   *
   * NOTE: This is undocumented behavior, but if you're receiving no results
   * from this function, you may need to pass in a 'sid' parameter matching a
   * location returned from getLocationList().
   *
   * @param array $recipes
   *   An array of recipes. For an examle of how these recipes should be
   *   constructed, see the output of getRecipeList().
   * @param array $params
   *   An optional array of parameters to filter the results set. The following
   *   additional parameters are available:
   *   - roundingmethod (string) The rounding method used for nutrients in the
   *     response. Allowed values are "raw" or "fda" (FDA-compliant). Default is
   *     raw.
   *
   * @return array
   *   An array representing the total nutrient summary for the given list of
   *   recipes. At the top level this has two elements: a "summary" array which
   *   has the total nutrient summary of every combined passed-in recipe, and a
   *   "recipes" array which has a per-recipe & quantity breakdown of the total
   *   nutrient values.
   */
  public function getTotalNutrientSummary(array $recipes, array $params = []) {
    $output = [];

    if (!isset($params['roundingmethod'])) {
      $params['roundingmethod'] = 'raw';
    }

    // First, we take our recipes array and convert it into the XML format
    // required by the API.
    $xml = '<TOTALNUTRIENTSUMMARYREQUEST roundingmethod="' . $params['roundingmethod'] . '"><RECIPES>';
    foreach ($recipes as $recipe) {
      $attributes = [
        'id' => 0,
        'reciperank' => 0,
        'menurank' => 0,
        'quantity' => 1,
        'portionnumerator' => 0,
        'portiondenominator' => 0,
      ];
      foreach ($recipe as $key => $value) {
        if (in_array($key, array_keys($attributes))) {
          // Sometimes the Computrition API returns recipe ID's in the format of
          // [recipe ID]-[menu rank]. If present, we simply strip away the dash
          // and everything after it.
          if ($key == 'id') {
            $value = substr($value, 0, strpos($value, '-'));
          }

          $attributes[$key] = $value;
        }
      }

      $attribute_string = '';
      foreach ($attributes as $key => $value) {
        $attribute_string .= $key . '="' . $value . '" ';
      }

      $xml .= "<RECIPE $attribute_string />";
    }
    $xml .= '</RECIPES></TOTALNUTRIENTSUMMARYREQUEST>';

    // Send it to the API, parse response, and return.
    if ($data = $this->execute('POST', 'total_nutrient_summary', $params, $xml)) {
      // If errors were returned, return them and abandon further processing.
      if ($data['error'] ?? NULL) {
        $output = ['error' => $data['error']];
        return $output;
      }

      // Parse the nutrient totals into a useful format.
      $output['summary'] = self::parseNutrients(
        $data['TOTALNUTRIENTSUMMARY']['NUTRIENTS'] ?? '',
        $data['TOTALNUTRIENTSUMMARY']['RECIPES']['@nutrienttotals'] ?? '',
        $data['TOTALNUTRIENTSUMMARY']['RECIPES']['@totalsuncertain'] ?? '',
        $data['TOTALNUTRIENTSUMMARY']['RECIPES']['@percentdailyvalue'] ?? ''
      );

      $output['recipes'] = $data['TOTALNUTRIENTSUMMARY']['RECIPES']['RECIPE'] ?? [];
      // Parse the nutrients of each recipe into a useful format.
      foreach ($output['recipes'] as $index => $recipe) {
        $output['recipes'][$index]['@nutrients'] = self::parseNutrients(
          $data['TOTALNUTRIENTSUMMARY']['NUTRIENTS'] ?? '',
          $recipe['@nutrients'] ?? '',
          $recipe['@nutrientsuncertain'] ?? '',
          $recipe['@percentdailyvalue'] ?? ''
        );
        unset($output['recipes'][$index]['@nutrientsuncertain']);
        unset($output['recipes'][$index]['@percentdailyvalue']);
        $output['recipes'][$index] = self::formatArrayKeys($output['recipes'][$index]);
      }
    }

    return $output;
  }

  /**
   * Send a request to the Computrition API and return the response.
   *
   * @param string $method
   *   The HTTP request method. This can be either GET or POST.
   * @param string $endpoint
   *   The API endpoint you are trying to hit. This is concatenated with the
   *   base URL defined in the module's settings.
   * @param array $params
   *   An optional array of parameters that are appended to the request as
   *   key-value pair query arguments. Additionally, you can set flags here
   *   to affect the behavior of the function:
   *     - 'return_errors':
   *         If TRUE, then any error responses will be returned in an array,
   *         instead of a boolean FALSE.
   * @param string $post_data
   *   If the method is POST, this data is sent as the body. It must be valid
   *   XML.
   * @param bool $reset
   *   If TRUE, all caches will be skipped and fresh data will be fetched for
   *   this request.
   *
   * @return array|bool
   *   If the request was successful, an array of data is returned. Otherwise,
   *   FALSE is returned.
   */
  protected function execute($method, $endpoint, array $params = [], $post_data = '', $reset = FALSE) {
    $cid = 'computrition_' . hash('crc32', __FUNCTION__ . $method . $endpoint . $post_data . json_encode($params));
    static $data = [];

    if ($return_errors = $params['return_errors'] ?? FALSE) {
      unset($params['return_errors']);
    }

    if (isset($data[$cid]) && !empty($data[$cid]) && !$reset) {
      $success = TRUE;
    }
    elseif (($cache = $this->cacheBackend->get($cid)) && !empty($cache->data) && !$reset) {
      $data[$cid] = Json::decode($cache->data, ['allowed_classes' => []]);
      $success = TRUE;
    }
    else {
      $base_url = $this->apiBaseUrl;
      if (!$base_url) {
        $message = 'Error in Computrition request: The API base URL has not been set in the module configuration.';
        $vars = [];
        $this->logger->error($message, $vars);
        return FALSE;
      }

      $url = $base_url . '/' . $endpoint;
      $options = ['timeout' => 120];
      $url = Url::fromUri($url, ['query' => $params])->toString();
      $success = FALSE;
      $data[$cid] = [];

      try {
        if ($method == 'POST') {
          $options['headers'] = ['Content-Type' => 'text/xml'];
          $options['body'] = $post_data;
          $response = $this->guzzleClient->post($url, $options);
        }
        else {
          $response = $this->guzzleClient->get($url, $options);
        }

        if ($response_data = $response->getBody()) {
          $xml = simplexml_load_string($response_data);
          $data[$cid] = self::xmlToArray($xml);
          // Every computrition response contains a 'status' element that
          // lets us know if the request was successful, and therefore worth
          // returning the data.
          if ($success = ($xml->STATUS['success'] == 1)) {
            $lifetime = $this->cacheLifetimeMinutes;
            if ($lifetime) {
              $expires = strtotime("+$lifetime minutes");
              $this->cacheBackend->set($cid, Json::encode($data[$cid]), $expires);
            }
          }
        }
      }
      catch (GuzzleException $e) {
        $message = 'Unable to complete computrition request. GuzzleException: <pre>@text</pre>';
        $vars = [
          '@text' => $e->getMessage(),
        ];
        $this->logger->error($message, $vars);
      }

      if (!$success) {
        $error_number = (string) ($xml->STATUS->ERROR['number'] ?? 0);
        $error_text = (string) ($xml->STATUS->ERROR['text'] ?? 'Request timed out or provided an invalid response.');

        if ($return_errors) {
          $data[$cid] = [
            'error' => [
              'number' => $error_number,
              'text' => $error_text,
            ],
          ];
        }
        if ($this->logBadResponses) {
          $message = 'Unable to complete computrition request. <pre>@req</pre>Response received: <pre>@text [code: @num]</pre>';
          $vars = [
            '@req' => $url,
            '@num' => $error_number,
            '@text' => $error_text,
          ];
          if ($severity = $this->responseCodesSeverity[$vars['@num']]) {
            $this->logger->log($severity, $message, $vars);
          }
          else {
            $this->logger->error($message, $vars);
          }
        }
      }
    }

    return ($success || $return_errors) ? $data[$cid] : FALSE;
  }

  /**
   * Convert a PHP SimpleXMLElement into an array.
   *
   * @param object $xml
   *   The SimpleXMLElement object to be converted into an array.
   * @param array $options
   *   An array of options to alter the resulting conversion array.
   *
   * @return array
   *   A PHP array representing the converted SimpleXMLElement object.
   *
   * @see http://outlandish.com/blog/xml-to-json/
   */
  protected static function xmlToArray($xml, array $options = []) {
    $defaults = [
      'namespaceSeparator' => ':',
      'attributePrefix' => '@',
      'alwaysArray' => [],
      'autoArray' => TRUE,
      'textContent' => '$',
      'autoText' => TRUE,
      'keySearch' => TRUE,
      'keyReplace' => TRUE,
    ];
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();

    // Add base (empty) namespace.
    $namespaces[''] = NULL;

    // Get attributes from all namespaces.
    $attributes_array = [];
    foreach ($namespaces as $prefix => $namespace) {
      foreach ($xml->attributes($namespace) as $attribute_name => $attribute) {
        // Replace characters in attribute name.
        if ($options['keySearch']) {
          $attribute_name = str_replace($options['keySearch'], $options['keyReplace'], $attribute_name);
        }
        $attributeKey = $options['attributePrefix'] . ($prefix ? $prefix . $options['namespaceSeparator'] : '') . $attribute_name;
        $attributes_array[$attributeKey] = (string) $attribute;
      }
    }

    // Get child nodes from all namespaces.
    $tags_array = [];
    foreach ($namespaces as $prefix => $namespace) {
      foreach ($xml->children($namespace) as $child_xml) {
        // Recurse into child nodes.
        $child_array = self::xmlToArray($child_xml, $options);
        $child_tag_name = key($child_array);
        $child_properties = current($child_array);

        // Replace characters in tag name.
        if ($options['keySearch']) {
          $child_tag_name = str_replace($options['keySearch'], $options['keyReplace'], $child_tag_name);
        }

        // Add namespace prefix, if any.
        if ($prefix) {
          $child_tag_name = $prefix . $options['namespaceSeparator'] . $child_tag_name;
        }

        if (!isset($tags_array[$child_tag_name])) {
          // Only entry with this key. test if tags of this type should always
          // be arrays, no matter the element count.
          $tags_array[$child_tag_name] = in_array($child_tag_name, $options['alwaysArray']) || !$options['autoArray'] ? [$child_properties] : $child_properties;
        }
        elseif (is_array($tags_array[$child_tag_name]) && array_keys($tags_array[$child_tag_name]) === range(0, count($tags_array[$child_tag_name]) - 1)) {
          // Key already exists and is integer indexed array.
          $tags_array[$child_tag_name][] = $child_properties;
        }
        else {
          // Key exists so convert to integer indexed array with previous value
          // in position 0.
          $tags_array[$child_tag_name] = [
            $tags_array[$child_tag_name],
            $child_properties,
          ];
        }
      }
    }

    // Get text content of node.
    $text_content_array = [];
    $plain_text = trim((string) $xml);
    if ($plain_text !== '') {
      $text_content_array[$options['textContent']] = $plain_text;
    }

    // Stick it all together.
    $properties_array = !$options['autoText'] || $attributes_array || $tags_array || ($plain_text === '') ? array_merge($attributes_array, $tags_array, $text_content_array) : $plain_text;

    // Return node as array.
    return [
      $xml->getName() => $properties_array,
    ];
  }

  /**
   * Parse an array of nutrient data into a more useful format.
   *
   * To reduce the size of returned XML results, Computrition returns nutrient
   * data in a very condensed manner. This function expands these concatenated
   * strings into useful arrays of nutrient data.
   *
   * @param string $nutrients
   *   A condensed string returned by the Computrition API representing the
   *   nutrients in a recipe, meal, etc.
   * @param string $values
   *   A string representing the amount of nutrients in a recipe. Each item in
   *   this list corresponds to each item in the main nutrients array.
   * @param string $uncertain
   *   A string representing the "The value of Nutrients Analysis Invalid for
   *   the recipe." Each item in this list corresponds to each item in the main
   *   nutrients array.
   * @param string $pdv
   *   A string representing the percent daily value of nutrients in a recipe.
   *   Each item in this list corresponds to each item in the main nutrients
   *   array.
   *
   * @return array
   *   A nicely formatted array of arrays representing each given nutrient.
   */
  protected static function parseNutrients($nutrients, $values = '', $uncertain = '', $pdv = '') {
    $output = [];
    $nutrients = explode('|', $nutrients);
    if (!empty($nutrients)) {
      foreach ($nutrients as $nutrient_string) {
        if ($nutrient_string) {
          $nutrient_array = [];
          $components = explode('~', $nutrient_string);
          $nutrient_array['shortname'] = $components[0];
          $nutrient_array['longname'] = $components[1];
          $nutrient_array['unit'] = $components[2];
          $output[] = $nutrient_array;
        }
      }
    }

    if ($values) {
      $values = explode('|', $values);
      foreach ($values as $index => $value) {
        if (is_numeric($value)) {
          $output[$index]['value'] = $value;
        }
      }
    }

    if ($uncertain) {
      $uncertain = explode('|', $uncertain);
      foreach ($uncertain as $index => $value) {
        if (is_numeric($value)) {
          $output[$index]['uncertain'] = $value;
        }
      }
    }

    if ($pdv) {
      $values = explode('|', $pdv);
      foreach ($values as $index => $value) {
        if (is_numeric($value)) {
          $output[$index]['percentdailyvalue'] = $value;
        }
      }
    }

    return $output;
  }

  /**
   * Cleans up the default output of xmlToArray().
   *
   * It removes attribute prefixes entirely and renames the array key for text
   * elements to 'name', as this is always the case for data returned from the
   * Computrition API.
   *
   * @param array $item
   *   An individual single-dimensional array of data output from the
   *   xmlToArray() function. If anything else is passed in, the
   *   function does nothing.
   *
   * @return array
   *   The $item array with cleaned up array keys.
   */
  protected static function formatArrayKeys(array $item) {
    if (!is_array($item)) {
      return $item;
    }

    $item_formatted = [];
    foreach ($item as $key => $value) {
      if ($key == '$') {
        $item_formatted['name'] = $value;
      }
      elseif (strpos($key, '@') === 0) {
        $updated_key = str_replace('@', '', $key);
        $item_formatted[$updated_key] = $value;
      }
    }

    return $item_formatted;
  }

}
