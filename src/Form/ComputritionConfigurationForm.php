<?php

namespace Drupal\computrition\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the Computrition API module settings.
 */
class ComputritionConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['computrition.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'computrition_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('computrition.settings');

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('API Configuration'),
      '#open' => TRUE,
    ];
    $form['api']['api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#default_value' => $config->get('api_base_url'),
      '#description' => $this->t('The base URL of the Hospitality Suite Web Service API. This generally looks something like "http://example.com:####/hsws".'),
      '#required' => TRUE,
    ];
    $form['api']['cache_lifetime_minutes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cache Lifetime'),
      '#default_value' => $config->get('cache_lifetime_minutes'),
      '#description' => $this->t('The amount of time (in minutes) that responses from the Computrition API will be cached. If set to 0, then database caching will be disabled for all Computrition API calls.'),
      '#required' => TRUE,
    ];
    $form['api']['log_bad_responses'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log unsuccessful computrition requests'),
      '#default_value' => $config->get('log_bad_responses'),
      '#description' => $this->t('If checked, any unsuccessful requests made to Computrition will be written to the database log with the returned response message and code.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_base_url = $form_state->getValue('api_base_url');
    if (!UrlHelper::isValid($api_base_url, TRUE)) {
      $message = $this->t('The given API url is not valid.');
      $form_state->setErrorByName('api_base_url', $message);
    }

    $cache_lifetime_minutes = $form_state->getValue('cache_lifetime_minutes');
    if (!(is_numeric($cache_lifetime_minutes) && $cache_lifetime_minutes >= 0)) {
      $message = $this->t('The minimum cache lifetime must be a non-negative number.');
      $form_state->setErrorByName('cache_lifetime_minutes', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('computrition.settings')
      ->set('api_base_url', $form_state->getValue('api_base_url'))
      ->set('cache_lifetime_minutes', $form_state->getValue('cache_lifetime_minutes'))
      ->set('log_bad_responses', $form_state->getValue('log_bad_responses'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
