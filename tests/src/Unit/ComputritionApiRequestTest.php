<?php

namespace Drupal\Tests\computrition\Unit;

use Drupal\computrition\Api\ComputritionApiRequest;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Tests the ComputritionApiRequest class methods.
 *
 * @group computrition
 */
class ComputritionApiRequestTest extends UnitTestCase {

  /**
   * Used for passing our test module configuration into the API constructor.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Cache backend required by the API constructor.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Guzzle http client required by the API constructor.
   *
   * @var \GuzzleHttp\Client
   */
  protected $guzzleClient;

  /**
   * Logger factory required by the API constructor.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Used for setting up our mocked guzzle HTTP requests to the external API.
   *
   * @var \GuzzleHttp\Handler\MockHandler
   */
  protected $guzzleMock;

  /**
   * An array of valid Computrition API responses for our mocked guzzle calls.
   *
   * @var string[]
   */
  protected $apiResponses;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

    // Mock the config.factory service and pass in some test settings. Because
    // the actual API responses are mocked these values are completely unused.
    $config_map = [
      'computrition.settings' => [
        'api_base_url' => 'https://example.com/hsws',
        'cache_lifetime_minutes' => 0,
      ],
    ];
    $this->configFactory = $this->getConfigFactoryStub($config_map);
    $container->set('config.factory', $this->configFactory);

    // Mock the cache.default service.
    $this->cacheBackend = $this->createMock('Drupal\Core\Cache\CacheBackendInterface');
    $container->set('cache.default', $this->cacheBackend);

    // Mock the unrouted_url_assembler service.
    $unrouted_url_assembler = $this->createMock('Drupal\Core\Utility\UnroutedUrlAssembler');
    $container->set('unrouted_url_assembler', $unrouted_url_assembler);

    // Mock the http_client service.
    $this->guzzleMock = new MockHandler([]);
    $handlerStack = HandlerStack::create($this->guzzleMock);
    $client = new Client(['handler' => $handlerStack]);
    $container->set('http_client', $client);
    $this->guzzleClient = $client;

    // Mock the logger.factory service.
    $this->logger = $this->createMock('Drupal\Core\Logger\LoggerChannelFactory');
    $container->set('logger.factory', $this->logger);

    // Define an array of valid XML API responses for each mocked guzzle
    // request. I'd prefer to load these out of files but unsure if that's
    // possible.
    $this->apiResponses = [
      'getLocationList' => '<?xml version="1.0" encoding="ISO-8859-1"?><HSWS><LOCATIONS><LOCATION description="" sid="alias01" /><LOCATION description="" sid="alias02" /></LOCATIONS><STATUS success="1" /></HSWS>',
      'getMealList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><MEALS><MEAL id="1" shortname="Breakfast" microname="BRK" starttime="070000" endtime="100000" interfacecode="BRK" isnourishmentmeal="0">Breakfast</MEAL><MEAL id="3" shortname="Lunch" microname="L" starttime="110000" endtime="143000" interfacecode="LUN" isnourishmentmeal="0">Lunch</MEAL><STATUS success="1"/></MEALS>',
      'getMenuTypeList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><MENUS><MENU id="6" shortname="GF" ispatronmenu="0" interfacecode="Gluten Free">Gluten Free</MENU><MENU id="10" shortname="Beverage" ispatronmenu="0" interfacecode="">Beverage</MENU><MENU id="13" shortname="Catering" ispatronmenu="0" interfacecode="">Catering</MENU><MENU id="5" shortname="Condiments" ispatronmenu="0" interfacecode="Condiments">Condiments</MENU><STATUS success="1"/></MENUS>',
      'getNutrientList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><NUTRIENTS><NUTRIENT interfacecode="caffeine" shortname="Caffeine">Caffeine</NUTRIENT><NUTRIENT interfacecode="calcium" shortname="Calcium">Calcium</NUTRIENT><STATUS success="1"/></NUTRIENTS>',
      'getPublishingGroupList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><PUBLISHING_GROUPS><GROUP id="33" shortname="Catering" maxselections="0" shownutrients="0" showimages="0">Catering</GROUP><GROUP id="9" shortname="Entrées" maxselections="0" shownutrients="0" showimages="0">Entrées</GROUP><STATUS success="1"/></PUBLISHING_GROUPS>',
      'getRecipeCategoryList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><CATEGORIES><CATEGORY id="36" shortname="Cld Cereal" parentid="31" fullpath="Breakfast Items:Cold Cereal" priceformula="" servicelocation="">Cold Cereal</CATEGORY><CATEGORY id="35" shortname="Hot Cereal" parentid="31" fullpath="Breakfast Items:Hot Cereal" priceformula="" servicelocation="">Hot Cereal</CATEGORY><STATUS success="1"/></CATEGORIES>',
      'getRecipeSourceList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><RECIPESOURCES><SOURCE id="44" shortname="Epicurious" parentid="" fullpath="Epicurious.com" authors="www">Epicurious.com</SOURCE><SOURCE id="47" shortname="Food.com" parentid="" fullpath="Food.com" authors="">Food.com</SOURCE><STATUS success="1"/></RECIPESOURCES>',
      'getMenuList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><PRODMENUS><MENU id="4500" shortname="Menu One" mealname="Lunch" mealid="3" servedate="20201021">Menu One</MENU><MENU id="4622" shortname="Menu Two" mealname="Dinner" mealid="5" servedate="20201021">Menu Two</MENU><STATUS success="1"/></PRODMENUS>',
      'getRecipeList' => '<?xml version="1.0" encoding="WINDOWS-1252"?><RECIPES><RECIPE id="2785" plucode="" category="Entrees:Pasta,Pizza" portionsize="1 each" portionnumerator="1" portiondenominator="1">Sausage Pizza</RECIPE><RECIPE id="581" plucode="" category="Starters:Salads" portionsize="4 ounces wt" portionnumerator="4" portiondenominator="1">Turkey Salad</RECIPE><STATUS success="1"/></RECIPES>',
      'getMenu' => '<PRODMENUS><MENU id="9759" servedate="20201021" mealid="7" mealname="Specials" menuname="Courtyard Specials" menuid="35" totalcount="0">Courtyard Specials</MENU><RECIPES><NUTRIENTS>Calories~Energy (kcal)~kcal|Cal/Fat~Calories from Fat~kcal|Fat~Fat~gm|SFA~Saturated Fats~gm|FA Trans~Fatty acids, total trans~gm|PUFA~Polyunsaturated Fats~gm|MUFA~Monounsaturated Fats~gm|Cholestrol~Cholesterol~mg|Sodium~Sodium~mg|Potassium~Potassium~mg|Carbs~Total Carbohydrates~gm|Fiber/Dtry~Dietary Fiber~gm|Sugars~Sugars~gm|Protein~Protein~gm|VitA (IU)~Vitamin A (IU)~IU|Vitamin C~Vitamin C (Ascorbic Acid)~mg|Calcium~Calcium~mg|Iron~Iron~mg|Thiamin~Vitamin B1-Thiamin~mg|Riboflavin~Vitamin B2-Riboflavin~mg|Niacin~Vitamin B3-Niacin~mg|Vitamin B6~Vitamin B6-Pyridoxine~mg|Folacin~Folacin~mcg|Vitamn B12~Vitamin B12-Cyanocobalamn~mcg|Phosphorus~Phosphorus~mg|Zinc~Zinc~mg|</NUTRIENTS><RECIPE id="441-300" name="Beef Barbacoa" plucode="" shortname="Beef Barbacoa" menurank="300" numservings="0" portion="4 ounces wt" portionnumerator="4" portiondenominator="1" portionweightgrams="113" isselected="0" ismainitem="1" group="In House Only" orderinggroup="(Unassigned)" orderingtext="" publishingdescription="Beef Barbacoa (GF)" publishingtext="" enticingdescription="" price="0" costperserving="1.24" category="Meat" productionarea="(Unassigned)" ingredientstext="CYC-Beef,Barbacoa,Cooked (Fully Cooked Beef With Solution Containing Up TO 14% Of A Solution Of Water, Orange Juice Concentrate,Spices, Flavoring,Salt,Sodium Phosphates,Lime Juice,Sodium Benzoate,Xanthan Gum. GLUTEN FREE.)" allergenstext="" nutrients="142|41|4.5|1.70|0.000|||60|822|454|2.7||1.2|22.6|301|4|17|3.57|||||||||" nutrientsuncertain="0|0|0|0|0|1|1|0|0|0|0|1|0|0|0|0|0|0|1|1|1|1|1|1|1|1|" percentdailyvalue="7||6|9||||20|36|10|1|||45|6|4|1|20|||||||||">Beef Barbacoa</RECIPE></RECIPES><STATUS success="1"/></PRODMENUS>',
      'getRecipe' => '<?xml version="1.0" encoding="WINDOWS-1252"?><RECIPES><NUTRIENTS>Calories~Energy (kcal)~kcal|Cal/Fat~Calories from Fat~kcal|Fat~Fat~gm|SFA~Saturated Fats~gm|FA Trans~Fatty acids, total trans~gm|PUFA~Polyunsaturated Fats~gm|MUFA~Monounsaturated Fats~gm|Cholestrol~Cholesterol~mg|Sodium~Sodium~mg|Potassium~Potassium~mg|Carbs~Total Carbohydrates~gm|Fiber/Dtry~Dietary Fiber~gm|Sugars~Sugars~gm|Protein~Protein~gm|VitA (IU)~Vitamin A (IU)~IU|Vitamin C~Vitamin C (Ascorbic Acid)~mg|Calcium~Calcium~mg|Iron~Iron~mg|Thiamin~Vitamin B1-Thiamin~mg|Riboflavin~Vitamin B2-Riboflavin~mg|Niacin~Vitamin B3-Niacin~mg|Vitamin B6~Vitamin B6-Pyridoxine~mg|Folacin~Folacin~mcg|Vitamn B12~Vitamin B12-Cyanocobalamn~mcg|Phosphorus~Phosphorus~mg|Zinc~Zinc~mg|</NUTRIENTS><RECIPE id="4774" plucode="" category="Entrees:Meatless:Vegan" portionsize="5 ounces wt" portionnumerator="5" portiondenominator="1" nutrients="235|183|20.7|1.57|.005|5.30|12.45|0|460|354|9.6|4.3|3.6|7.4|857|7|79|1.59|.19|.29|1.8|.096|139|0.00|165|1.25|" nutrientsuncertain="0|0|0|0|1|1|1|1|1|1|0|1|1|0|1|1|1|1|1|1|1|1|1|1|1|1|">Asparagus with Almonds &amp; Goji Berries</RECIPE><STATUS success="1"/></RECIPES>',
      'getTotalNutrientSummary' => '<?xml version="1.0" encoding="ISO-8859-1"?><TOTALNUTRIENTSUMMARY roundingmethod="raw"><NUTRIENTS>Calories~Energy (kcal)~kcal|Cal/Fat~Calories from Fat~kcal|Fat~Fat~gm|SFA~Saturated Fats~gm|FA Trans~Fatty acids, total trans~gm|PUFA~Polyunsaturated Fats~gm|MUFA~Monounsaturated Fats~gm|Cholestrol~Cholesterol~mg|Sodium~Sodium~mg|Potassium~Potassium~mg|Carbs~Total Carbohydrates~gm|Fiber/Dtry~Dietary Fiber~gm|Sugars~Sugars~gm|Protein~Protein~gm|VitA (IU)~Vitamin A (IU)~IU|Vitamin C~Vitamin C (Ascorbic Acid)~mg|Calcium~Calcium~mg|Iron~Iron~mg|Thiamin~Vitamin B1-Thiamin~mg|Riboflavin~Vitamin B2-Riboflavin~mg|Niacin~Vitamin B3-Niacin~mg|Vitamin B6~Vitamin B6-Pyridoxine~mg|Folacin~Folacin~mcg|Vitamn B12~Vitamin B12-Cyanocobalamn~mcg|Phosphorus~Phosphorus~mg|Zinc~Zinc~mg|</NUTRIENTS><RECIPES nutrienttotals="56281|30863|3442.1|2000.18|89.831|162.15|848.91|10276|85823|44293|4811.5|147.9|2173.8|1531.0|150024|867|30232|209.25|38.48|60.41|258.2|17.021|7848|131.19|27531|173.74|" totalsuncertain="0|1|0|1|1|1|1|1|0|1|0|1|1|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="2814||4413|10001||||3425|3731|942|1750|528||3062|3000|963|2326|1163|3207|4647|1614|1001|1962|5466|2203|1579|"><RECIPE id="1427" reciperank="0" menurank="0" nutrients="241|207|23.0|.04|0.000|.14|.06|14|406|37|.9|.6|.1|.2|566|0|8|.37|.01|.02|.2|.037|0|0.00|11|.08|" nutrientsuncertain="0|0|0|1|1|1|1|0|0|1|0|1|0|0|1|1|0|0|1|1|1|1|1|1|1|1|" percentdailyvalue="12||29|0||||5|18|1|0|2||0|11|0|1|2|1|2|1|2|0|0|1|1|"/><RECIPE id="345" reciperank="0" menurank="1" nutrients="960|360|40.0|18.79|0.000|2.56|14.34|101|5851|1749|123.0|16.5|12.5|39.5|23592|295|1097|11.72|.87|1.42|9.6|1.188|372|1.92|846|4.96|" nutrientsuncertain="0|0|0|1|1|1|1|1|0|1|0|1|1|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="48||51|94||||34|254|37|45|59||79|472|328|84|65|73|109|60|70|93|80|68|45|"/><RECIPE id="1298" reciperank="0" menurank="2" nutrients="43864|27077|3008.5|1839.62|89.213|142.08|769.82|8194|62730|34661|3214.2|79.9|1193.4|1049.4|106020|5|27628|137.95|32.46|54.03|183.8|9.815|6271|110.03|23479|109.69|" nutrientsuncertain="0|1|0|0|1|1|1|0|0|1|0|0|1|0|0|0|0|0|1|1|1|1|1|1|1|1|" percentdailyvalue="2193||3857|9198||||2731|2727|737|1169|285||2099|2120|5|2125|766|2705|4156|1149|577|1568|4585|1878|997|"/><RECIPE id="1428" reciperank="0" menurank="3" nutrients="39|2|.2|.06|0.000|.09|0.00|0|18|303|9.3|1.0|8.9|1.0|3835|42|10|.24|.05|.02|.8|.082|24|0.00|17|.20|" nutrientsuncertain="0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|" percentdailyvalue="2||0|0||||0|1|6|3|4||2|77|46|1|1|4|2|5|5|6|0|1|2|"/><RECIPE id="515" reciperank="0" menurank="4" nutrients="2749|73|8.1|.45|0.000|2.26|5.04|0|205|1243|675.9|2.7|662.6|6.7|7|2|641|7.37|.18|.06|1.8|.368|43|0.00|232|1.66|" nutrientsuncertain="0|0|0|0|1|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|" percentdailyvalue="137||10|2||||0|9|26|246|10||13|0|2|49|41|15|4|11|22|11|0|19|15|"/><RECIPE id="6729" reciperank="0" menurank="5" nutrients="1680|576|64.0|40.00||||380|1400||248.0|4.0|188.0|20.0|||||||||||||" nutrientsuncertain="0|0|0|0|1|1|1|0|0|1|0|0|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="84||82|200||||127|61||90|14||40|||||||||||||"/><RECIPE id="3612" reciperank="0" menurank="6" nutrients="1934|953|105.9|40.57|.552|5.48|52.95|733|2588|3006|14.8|.6|.3|203.4|407|0|100|25.51|.99|2.01|29.3|3.012|96|18.84|1807|47.61|" nutrientsuncertain="0|1|0|0|1|1|1|0|0|1|0|0|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="97||136|203||||244|113|64|5|2||407|8|0|8|142|82|155|183|177|24|785|145|433|"/><RECIPE id="1765" reciperank="0" menurank="7" nutrients="2162|306|34.3|5.03|.023|4.02|1.68|290|5935|886|339.5|15.3|29.4|102.0|1257|18|257|18.27|3.29|1.97|22.6|.603|591|0.00|515|5.00|" nutrientsuncertain="0|0|0|0|1|1|1|0|0|1|0|0|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="108||44|25||||97|258|19|123|54||204|25|20|20|101|274|152|141|35|148|0|41|45|"/><RECIPE id="1399" reciperank="0" menurank="8" nutrients="71|23|2.5|.31|0.000|1.01|.62|0|395|568|10.4|4.3|2.6|5.2|11329|136|188|1.93|.14|.15|1.2|.326|162|0.00|116|.78|" nutrientsuncertain="0|1|0|0|1|1|1|0|0|1|0|0|0|0|0|0|0|0|1|1|1|1|1|1|1|1|" percentdailyvalue="4||3|2||||0|17|12|4|15||10|227|151|14|11|12|12|8|19|40|0|9|7|"/><RECIPE id="3393" reciperank="0" menurank="9" nutrients="129|17|1.9|.25|0.000|.58|.10|0|109|1039|24.4|11.1|8.6|10.6|1440|303|143|2.38|.26|.38|2.4|.789|243|0.00|222|1.32|" nutrientsuncertain="0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|" percentdailyvalue="6||2|1||||0|5|22|9|40||21|29|336|11|13|21|29|15|46|61|0|18|12|"/><RECIPE id="1425" reciperank="0" menurank="10" nutrients="228|118|13.0|3.96|.044|3.55|4.21|134|183|346|4.2|1.4|.6|24.1|521|6|40|2.33|.10|.19|5.7|.458|11|.40|211|1.97|" nutrientsuncertain="0|1|0|0|1|1|1|0|0|1|0|1|1|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="11||17|20||||45|8|7|2|5||48|10|7|3|13|8|15|35|27|3|17|17|18|"/><RECIPE id="3176" reciperank="0" menurank="11" nutrients="539|272|43.5|14.05|0.000|.05|.02|113|1487|244|13.8|2.6|5.4|10.6|233|57|27|.41|.08|.04|.4|.251|24|0.00|39|.24|" nutrientsuncertain="0|0|0|0|1|1|1|0|0|1|0|0|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="27||56|70||||38|65|5|5|9||21|5|63|2|2|7|3|3|15|6|0|3|2|"/><RECIPE id="1426" reciperank="0" menurank="12" nutrients="790|321|35.3|3.20|0.000|.34|.07|107|3799|211|59.6|5.7|3.7|44.4|816|3|92|.79|.07|.11|.4|.093|12|0.00|36|.23|" nutrientsuncertain="0|0|0|1|1|1|1|0|0|1|0|1|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="40||45|16||||36|165|4|22|20||89|16|4|7|4|6|8|2|5|3|0|3|2|"/><RECIPE id="355" reciperank="0" menurank="13" nutrients="896|558|61.7|33.86||||209|717||73.7|2.0|57.8|13.9|||||||||||||" nutrientsuncertain="0|0|0|0|1|1|1|0|0|1|0|0|0|0|1|1|1|1|1|1|1|1|1|1|1|1|" percentdailyvalue="45||79|169||||70|31||27|7||28|||||||||||||"/></RECIPES><STATUS success="1"/></TOTALNUTRIENTSUMMARY>',
    ];
  }

  /**
   * Tests the ComputritionApiRequest::getLocationList() function.
   */
  public function testGetLocationList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getLocationList']));
    $location_list = $api->getLocationList();

    $this->assertTrue(!empty($location_list), 'Fetched a list of locations.');
  }

  /**
   * Tests the ComputritionApiRequest::getMealList() function.
   */
  public function testGetMealList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMealList']));
    $list = $api->getMealList();

    $this->assertTrue(!empty($list), 'Fetched a list of meals.');
  }

  /**
   * Tests the ComputritionApiRequest::getMenuTypeList() function.
   */
  public function testGetMenuTypeList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenuTypeList']));
    $list = $api->getMenuTypeList();

    $this->assertTrue(!empty($list), 'Fetched a list of menu types.');
  }

  /**
   * Tests the ComputritionApiRequest::getNutrientList() function.
   */
  public function testGetNutrientList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getNutrientList']));
    $list = $api->getNutrientList();

    $this->assertTrue(!empty($list), 'Fetched a list of nutrients.');
  }

  /**
   * Tests the ComputritionApiRequest::getPublishingGroupList() function.
   */
  public function testGetPublishingGroupList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getPublishingGroupList']));
    $list = $api->getPublishingGroupList();

    $this->assertTrue(!empty($list), 'Fetched a list of publishing groups.');
  }

  /**
   * Tests the ComputritionApiRequest::getRecipeCategoryList() function.
   */
  public function testGetRecipeCategoryList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getRecipeCategoryList']));
    $list = $api->getRecipeCategoryList();

    $this->assertTrue(!empty($list), 'Fetched a list of recipe categories.');
  }

  /**
   * Tests the ComputritionApiRequest::getRecipeSourceList() function.
   */
  public function testGetRecipeSourceList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getRecipeSourceList']));
    $list = $api->getRecipeSourceList();

    $this->assertTrue(!empty($list), 'Fetched a list of recipe sources.');
  }

  /**
   * Tests the ComputritionApiRequest::getMenuList() function.
   */
  public function testGetMenuList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getLocationList']));
    $location_list = $api->getLocationList();

    $today = date('Ymd');
    $random_location = !empty($location_list) ? $location_list[array_rand($location_list)] : [];
    $menu_params = [
      'sid' => $random_location['sid'],
    ];
    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenuList']));
    $menu_list = $api->getMenuList($today, $menu_params);

    $this->assertTrue(!empty($menu_list), 'Fetched a list of menus.');
  }

  /**
   * Tests the ComputritionApiRequest::getRecipeList() function.
   */
  public function testGetRecipeList() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getRecipeList']));
    $recipe_list = $api->getRecipeList();

    $this->assertTrue(!empty($recipe_list), 'Fetched a list of recipes.');
  }

  /**
   * Tests the ComputritionApiRequest::getMenu() function.
   */
  public function testGetMenu() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getLocationList']));
    $location_list = $api->getLocationList();

    $today = date('Ymd');
    $random_location = !empty($location_list) ? $location_list[array_rand($location_list)] : [];
    $menu_params = [
      'sid' => $random_location['sid'],
    ];
    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenuList']));
    $menu_list = $api->getMenuList($today, $menu_params);

    $random_menu = !empty($menu_list) ? $menu_list[array_rand($menu_list)] : [];
    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenu']));
    $menu = $api->getMenu($random_menu['id'] ?? 0, $menu_params);
    $this->assertTrue(!empty($menu), 'Fetched a specific menu.');
  }

  /**
   * Tests the ComputritionApiRequest::getRecipe() function.
   */
  public function testGetRecipe() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getRecipeList']));
    $recipe_list = $api->getRecipeList();

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getRecipe']));
    $random_recipe = !empty($recipe_list) ? $recipe_list[array_rand($recipe_list)] : [];
    $recipe = $api->getRecipe($random_recipe['id'] ?? 0);

    $this->assertTrue(!empty($recipe), 'Fetched a specific recipe.');
  }

  /**
   * Tests the ComputritionApiRequest::getTotalNutrientSummary() function.
   */
  public function testGetTotalNutrientSummary() {
    $api = new ComputritionApiRequest($this->configFactory, $this->cacheBackend, $this->guzzleClient, $this->logger);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getLocationList']));
    $location_list = $api->getLocationList();

    $today = date('Ymd');
    $random_location = !empty($location_list) ? $location_list[array_rand($location_list)] : [];
    $menu_params = [
      'sid' => $random_location['sid'],
    ];
    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenuList']));
    $menu_list = $api->getMenuList($today, $menu_params);

    $random_menu = !empty($menu_list) ? $menu_list[array_rand($menu_list)] : [];
    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getMenu']));
    $menu = $api->getMenu($random_menu['id'] ?? 0, $menu_params);

    $this->guzzleMock->append(new Response(200, ['Content-Type' => 'text/xml'], $this->apiResponses['getTotalNutrientSummary']));
    $recipes = $menu['recipes'] ?? [];
    $recipes[0]['quantity'] = 3;
    $summary = $api->getTotalNutrientSummary($recipes, $menu_params);

    $this->assertTrue(!empty($summary), 'Fetched a total nutrient summary.');
  }

}
