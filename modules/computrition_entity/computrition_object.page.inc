<?php

/**
 * @file
 * Contains computrition_object.page.inc.
 *
 * Page callback for Computrition object entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Computrition object templates.
 *
 * Default template: computrition_object.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_computrition_object(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
