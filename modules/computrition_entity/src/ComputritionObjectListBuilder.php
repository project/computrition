<?php

namespace Drupal\computrition_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Computrition object entities.
 *
 * @ingroup computrition_entity
 */
class ComputritionObjectListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Entity ID');
    $header['remote_id'] = $this->t('Remote ID');
    $header['type'] = $this->t('Type');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\computrition_entity\Entity\ComputritionObject $entity */
    $row['id'] = $entity->id();
    $row['remote_id'] = $entity->get('remote_id')->value;

    $bundle = \Drupal::entityTypeManager()
      ->getStorage('computrition_object_type')
      ->load($entity->bundle())
      ->label();
    $row['type'] = $bundle;

    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.computrition_object.canonical',
      ['computrition_object' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
