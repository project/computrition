<?php

namespace Drupal\computrition_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Computrition object entities.
 *
 * @ingroup computrition_entity
 */
interface ComputritionObjectInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Computrition object name.
   *
   * @return string
   *   Name of the Computrition object.
   */
  public function getName();

  /**
   * Sets the Computrition object name.
   *
   * @param string $name
   *   The Computrition object name.
   *
   * @return \Drupal\computrition_entity\Entity\ComputritionObjectInterface
   *   The called Computrition object entity.
   */
  public function setName($name);

  /**
   * Gets the Computrition object creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Computrition object.
   */
  public function getCreatedTime();

  /**
   * Sets the Computrition object creation timestamp.
   *
   * @param int $timestamp
   *   The Computrition object creation timestamp.
   *
   * @return \Drupal\computrition_entity\Entity\ComputritionObjectInterface
   *   The called Computrition object entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Computrition object remote computrition ID.
   *
   * @return string
   *   Remote ID of the Computrition object.
   */
  public function getRemoteId();

  /**
   * Sets the Computrition object remote computrition ID.
   *
   * @param string $id
   *   The Computrition object remote ID.
   *
   * @return \Drupal\computrition_entity\Entity\ComputritionObjectInterface
   *   The called Computrition object entity.
   */
  public function setRemoteId($id);

  /**
   * Gets the Computrition object remote data.
   *
   * @return string
   *   Remote data of the Computrition object.
   */
  public function getRemoteData();

  /**
   * Sets the Computrition object remote data.
   *
   * @param string $data
   *   The Computrition object remote data.
   *
   * @return \Drupal\computrition_entity\Entity\ComputritionObjectInterface
   *   The called Computrition object entity.
   */
  public function setRemoteData($data);

}
