<?php

namespace Drupal\computrition_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Computrition object type entity.
 *
 * @ConfigEntityType(
 *   id = "computrition_object_type",
 *   label = @Translation("Computrition object type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\computrition_entity\ComputritionObjectTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\computrition_entity\Form\ComputritionObjectTypeForm",
 *       "edit" = "Drupal\computrition_entity\Form\ComputritionObjectTypeForm",
 *       "delete" = "Drupal\computrition_entity\Form\ComputritionObjectTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\computrition_entity\ComputritionObjectTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "computrition_object_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "computrition_object",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/computrition_object_type/{computrition_object_type}",
 *     "add-form" = "/admin/structure/computrition_object_type/add",
 *     "edit-form" = "/admin/structure/computrition_object_type/{computrition_object_type}/edit",
 *     "delete-form" = "/admin/structure/computrition_object_type/{computrition_object_type}/delete",
 *     "collection" = "/admin/structure/computrition_object_type"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *   }
 * )
 */
class ComputritionObjectType extends ConfigEntityBundleBase implements ComputritionObjectTypeInterface {

  /**
   * The Computrition object type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Computrition object type label.
   *
   * @var string
   */
  protected $label;

}
