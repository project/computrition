<?php

namespace Drupal\computrition_entity\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Computrition object entity.
 *
 * @ingroup computrition_entity
 *
 * @ContentEntityType(
 *   id = "computrition_object",
 *   label = @Translation("Computrition object"),
 *   bundle_label = @Translation("Computrition object type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\computrition_entity\ComputritionObjectListBuilder",
 *     "views_data" = "Drupal\computrition_entity\Entity\ComputritionObjectViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\computrition_entity\Form\ComputritionObjectForm",
 *       "add" = "Drupal\computrition_entity\Form\ComputritionObjectForm",
 *       "edit" = "Drupal\computrition_entity\Form\ComputritionObjectForm",
 *       "delete" = "Drupal\computrition_entity\Form\ComputritionObjectDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\computrition_entity\ComputritionObjectHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\computrition_entity\ComputritionObjectAccessControlHandler",
 *   },
 *   base_table = "computrition_object",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer computrition object entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/computrition_object/{computrition_object}",
 *     "add-page" = "/computrition_object/add",
 *     "add-form" = "/computrition_object/add/{computrition_object_type}",
 *     "edit-form" = "/computrition_object/{computrition_object}/edit",
 *     "delete-form" = "/computrition_object/{computrition_object}/delete",
 *     "collection" = "/admin/content/computrition_object",
 *   },
 *   bundle_entity_type = "computrition_object_type",
 *   field_ui_base_route = "entity.computrition_object_type.edit_form"
 * )
 */
class ComputritionObject extends ContentEntityBase implements ComputritionObjectInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($id) {
    $this->set('remote_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteData() {
    return $this->get('remote_data')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteData($data) {
    $this->set('remote_data', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Computrition object entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setDescription(t('The ID of this object in the external Computrition system.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['remote_data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Remote Data'))
      ->setDescription(t('The complete returned raw data of this object from the external Computrition system.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
