<?php

namespace Drupal\computrition_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Computrition object type entities.
 */
interface ComputritionObjectTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
