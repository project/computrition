<?php

namespace Drupal\computrition_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Computrition object entities.
 *
 * @ingroup computrition_entity
 */
class ComputritionObjectDeleteForm extends ContentEntityDeleteForm {


}
