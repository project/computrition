<?php

namespace Drupal\computrition_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form for configuration of the computrition object bundle types.
 */
class ComputritionObjectTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $computrition_object_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $computrition_object_type->label(),
      '#description' => $this->t("Label for the Computrition object type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $computrition_object_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\computrition_entity\Entity\ComputritionObjectType::load',
      ],
      '#disabled' => !$computrition_object_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $computrition_object_type = $this->entity;
    $status = $computrition_object_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Computrition object type.', [
          '%label' => $computrition_object_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Computrition object type.', [
          '%label' => $computrition_object_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($computrition_object_type->toUrl('collection'));
  }

}
