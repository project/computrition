<?php

namespace Drupal\computrition_entity;

use Drupal\computrition\Api\ComputritionApiRequest;
use Drupal\computrition\Api\ComputritionErrorResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;

/**
 * Delete drupal entities that reference deleted computrition objects.
 */
class ComputritionEntityPurger {

  /**
   * Computrition API request object used for fetching data from Computrition.
   *
   * @var \Drupal\computrition\Api\ComputritionApiRequest
   */
  protected $api;

  /**
   * Entity type manager used for deleting our imported drupal entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Lock used to prevent multiple purges from running concurrently.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Logger factory used for writing to the Drupal database log.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Module handler used for making our purge functions hookable.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The user-defined module configuration options.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ComputritionEntityPurger constructor.
   *
   * @param \Drupal\computrition\Api\ComputritionApiRequest $api
   *   The computrition API request object used for pulling data from the
   *   external computrition API.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager used for deleting our imported drupal entities.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   DatabaseLockBackend used to prevent multiple purges from running
   *   concurrently and trampling each other.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger factory used for writing messages to the Drupal database log.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   Module handler used for making our purge functions hookable.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The user-defined module configuration options.
   */
  public function __construct(ComputritionApiRequest $api, EntityTypeManagerInterface $entity_type_manager, LockBackendInterface $lock, LoggerChannelFactory $logger, ModuleHandler $module_handler, ConfigFactoryInterface $config_factory) {
    $this->api = $api;
    $this->entityTypeManager = $entity_type_manager;
    $this->lock = $lock;
    $this->logger = $logger->get('computrition_entity');
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('computrition.settings');
  }

  /**
   * Purge orphaned computrition object entities.
   *
   * @param string $bundle
   *   If provided, only orphaned computrition objects of this bundle type will
   *   be deleted.
   */
  public function purgeOrphanObjects($bundle = NULL) {
    $key = 'computrition_entity_purge_' . ($bundle ?? 'all');
    if (!$this->lock->acquire($key)) {
      $tArgs = [':type' => $bundle];
      $this->logger->notice('Unable to purge computrition :type objects, it is already running.', $tArgs);
      return FALSE;
    }

    $query = $this->entityTypeManager->getStorage('computrition_object')->getQuery();
    if ($bundle) {
      $query->condition('type', $bundle);
    }

    $ids = $query->accessCheck(FALSE)->execute();
    foreach ($ids as $id) {
      $this->deleteIfOrphan($id);
      gc_collect_cycles();
    }

    $this->lock->release($key);
  }

  /**
   * Delete a drupal entity if it has been orphaned in computrition.
   *
   * @param int $id
   *   A drupal entity id for a computrition object.
   */
  protected function deleteIfOrphan(int $id) {
    $object = $this->entityTypeManager->getStorage('computrition_object')->load($id);
    if ($remoteId = $object->getRemoteId()) {
      switch ($object->bundle()) {
        case 'location':
          $sids = $this->getLocationSids();
          if (!empty($sids) && !in_array($remoteId, $sids)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition location "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'meal':
          if ($locationId = $object->get('location')->getValue()[0]['target_id'] ?? NULL) {
            $location = $this->entityTypeManager->getStorage('computrition_object')->load($locationId);
            $locationId = $location->get('remote_id')->getValue()[0]['value'] ?? '';
          }
          $mealIds = $this->getMealIds($locationId);
          if (!empty($mealIds) && !in_array($remoteId, $mealIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition meal "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'menu_type':
          if ($locationId = $object->get('location')->getValue()[0]['target_id'] ?? NULL) {
            $location = $this->entityTypeManager->getStorage('computrition_object')->load($locationId);
            $locationId = $location->get('remote_id')->getValue()[0]['value'] ?? '';
          }
          $menuTypeIds = $this->getMenuTypeIds($locationId);
          if (!empty($menuTypeIds) && !in_array($remoteId, $menuTypeIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition menu type "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'menu':
          $reqParams = ['return_errors' => TRUE];
          if ($locationId = $object->get('location')->getValue()[0]['target_id'] ?? NULL) {
            $location = $this->entityTypeManager->getStorage('computrition_object')->load($locationId);
            if ($locationId = $location->get('remote_id')->getValue()[0]['value'] ?? '') {
              $reqParams['sid'] = $locationId;
            }
          }
          $apiResponse = $this->api->getMenu($remoteId, $reqParams);
          if ($errorCode = $apiResponse['error']['number'] ?? NULL) {
            if ($errorCode == ComputritionErrorResponse::NOT_FOUND) {
              $tArgs = [
                '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
                '@id' => $object->id(),
                '@rid' => $remoteId,
              ];
              $this->logger->notice('Deleted orphaned Computrition menu "@title" [id: @id] [remote_id: @rid].', $tArgs);
              $object->delete();
            }
          }
          break;

        case 'nutrient':
          $nutrientIds = $this->getNutrientIds();
          if (!empty($nutrientIds) && !in_array($remoteId, $nutrientIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition nutrient "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'publishing_group':
          $pubGroupIds = $this->getPubGroupIds();
          if (!empty($pubGroupIds) && !in_array($remoteId, $pubGroupIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition publishing group "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'recipe_category':
          $recipeCategoryIds = $this->getRecipeCategoryIds();
          if (!empty($recipeCategoryIds) && !in_array($remoteId, $recipeCategoryIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition recipe category "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'recipe_source':
          $recipeSourceIds = $this->getRecipeSourceIds();
          if (!empty($recipeSourceIds) && !in_array($remoteId, $recipeSourceIds)) {
            $tArgs = [
              '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
              '@id' => $object->id(),
              '@rid' => $remoteId,
            ];
            $this->logger->notice('Deleted orphaned Computrition recipe source "@title" [id: @id] [remote_id: @rid].', $tArgs);
            $object->delete();
          }
          break;

        case 'recipe':
          $reqParams = ['return_errors' => TRUE];
          if ($locationId = $object->get('location')->getValue()[0]['target_id'] ?? NULL) {
            $location = $this->entityTypeManager->getStorage('computrition_object')->load($locationId);
            if ($locationId = $location->get('remote_id')->getValue()[0]['value'] ?? '') {
              $reqParams['sid'] = $locationId;
            }
          }
          $apiResponse = $this->api->getRecipe($remoteId, $reqParams);
          if ($errorCode = $apiResponse['error']['number'] ?? NULL) {
            if ($errorCode == ComputritionErrorResponse::NOT_FOUND) {
              $tArgs = [
                '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
                '@id' => $object->id(),
                '@rid' => $remoteId,
              ];
              $this->logger->notice('Deleted orphaned Computrition recipe "@title" [id: @id] [remote_id: @rid].', $tArgs);
              $object->delete();
            }
          }
          break;

        default:
          // Computrition object of an unknown type.
          $tArgs = [
            '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
            '@id' => $object->id(),
            '@rid' => $remoteId,
          ];
          $this->logger->notice('Deleted invalid Computrition object "@title" [id: @id] [remote_id: @rid].', $tArgs);
          $object->delete();
          break;
      }
    }
    elseif (!is_numeric($remoteId)) {
      // Computrition object with no remote ID.
      $tArgs = [
        '@title' => $object->get('name')->getValue()[0]['value'] ?? '',
        '@id' => $object->id(),
        '@rid' => $remoteId,
      ];
      $this->logger->notice('Deleted invalid Computrition object "@title" [id: @id] [remote_id: @rid].', $tArgs);
      $object->delete();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Return an array of all location schema IDs in Computrition.
   *
   * @return string[]
   *   An array of computrition location schema IDs.
   */
  protected function getLocationSids() {
    $output = &drupal_static(__FUNCTION__);
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $locations = $this->api->getLocationList();

    foreach ($locations as $location) {
      $output[$location['sid']] = $location['sid'];
    }

    return $output;
  }

  /**
   * Return an array of all meal IDs in Computrition for a given location.
   *
   * @param string $locationId
   *   The remote ID of a location in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getMealIds(string $locationId) {
    $output = &drupal_static(__FUNCTION__ . '_' . ($locationId ?? ''));
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $reqParams = [];
    if ($locationId) {
      $reqParams['sid'] = $locationId;
    }
    $meals = $this->api->getMealList($reqParams);

    foreach ($meals as $meal) {
      $output[$meal['id']] = $meal['id'];
    }

    return $output;
  }

  /**
   * Return an array of all menu type IDs in Computrition for a given location.
   *
   * @param string $locationId
   *   The remote ID of a location in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getMenuTypeIds(string $locationId) {
    $output = &drupal_static(__FUNCTION__ . '_' . ($locationId ?? ''));
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $reqParams = [];
    if ($locationId) {
      $reqParams['sid'] = $locationId;
    }
    $types = $this->api->getMenuTypeList($reqParams);

    foreach ($types as $type) {
      $output[$type['id']] = $type['id'];
    }

    return $output;
  }

  /**
   * Return an array of all nutrient IDs in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getNutrientIds() {
    $output = &drupal_static(__FUNCTION__);
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $nutrients = $this->api->getNutrientList();

    foreach ($nutrients as $nutrient) {
      $output[$nutrient['interfacecode']] = $nutrient['interfacecode'];
    }

    return $output;
  }

  /**
   * Return an array of all publishing group IDs in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getPubGroupIds() {
    $output = &drupal_static(__FUNCTION__);
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $pubGroups = $this->api->getPublishingGroupList();

    foreach ($pubGroups as $group) {
      $output[$group['id']] = $group['id'];
    }

    return $output;
  }

  /**
   * Return an array of all recipe category IDs in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getRecipeCategoryIds() {
    $output = &drupal_static(__FUNCTION__);
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $recipeCategories = $this->api->getPublishingGroupList();

    foreach ($recipeCategories as $category) {
      $output[$category['id']] = $category['id'];
    }

    return $output;
  }

  /**
   * Return an array of all recipe source IDs in Computrition.
   *
   * @return int[]
   *   An array of numeric IDs.
   */
  protected function getRecipeSourceIds() {
    $output = &drupal_static(__FUNCTION__);
    if (!empty($output)) {
      return $output;
    }

    $output = [];
    $recipeSources = $this->api->getRecipeSourceList();

    foreach ($recipeSources as $source) {
      $output[$source['id']] = $source['id'];
    }

    return $output;
  }

}
