<?php

namespace Drupal\computrition_entity\Commands;

use Drupal\computrition_entity\ComputritionEntityImporter;
use Drupal\computrition_entity\ComputritionEntityPurger;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ComputritionEntityCommands extends DrushCommands {

  /**
   * Computrition entity importer.
   *
   * @var \Drupal\computrition_entity\ComputritionEntityImporter
   */
  protected $importer;

  /**
   * Computrition entity orphan object purger.
   *
   * @var \Drupal\computrition_entity\ComputritionEntityPurger
   */
  protected $purger;

  /**
   * Lock used to prevent multiple imports from running concurrently.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Entity type manager used for creating, loading, and deleting entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State object for tracking when the last import ran.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * ComputritionEntityCommands constructor.
   *
   * @param \Drupal\computrition\Api\ComputritionEntityImporter $importer
   *   Computrition entity importer.
   * @param \Drupal\computrition\Api\ComputritionEntityPurger $purger
   *   Computrition entity orphan object purger.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   DatabaseLockBackend used to prevent multiple imports from running
   *   concurrently and trampling each other.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager used for creating, loading, and deleting Drupal
   *   entities.
   * @param Drupal\Core\State\StateInterface $state
   *   State object for tracking when the last import ran.
   */
  public function __construct(ComputritionEntityImporter $importer, ComputritionEntityPurger $purger, LockBackendInterface $lock, EntityTypeManagerInterface $entity_type_manager, StateInterface $state) {
    parent::__construct();
    $this->importer = $importer;
    $this->purger = $purger;
    $this->lock = $lock;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
  }

  /**
   * Run a full import of all computrition objects into drupal entities.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option skip-hash-check
   *   If provided, then existing entities will be force-reimported even if
   *   they haven't been modified.
   *
   * @command computrition_entity:import-all
   * @aliases ceia
   */
  public function importAll(array $options = ['skip-hash-check' => FALSE]) {
    $this->output()->write(dt('Beginning full computrition entity import... '));
    $start = time();

    if (!$this->lock->acquire('computrition_entity_import_all')) {
      $this->output()->write(dt("Error!\r\n"));
      $this->output()->writeln(dt('Unable to acquire lock. The full importer is already running.'));
      return FALSE;
    }
    $this->output()->write(dt("Ok!\r\n"));

    $params = [
      'skip_hash_check' => $options['skip-hash-check'] ?? FALSE,
    ];

    $this->output()->write(dt('Importing locations... '));
    $this->importer->importLocations($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing meals... '));
    $this->importer->importMeals($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing menu types... '));
    $this->importer->importMenuTypes($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing nutrients... '));
    $this->importer->importNutrients($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing publishing groups... '));
    $this->importer->importPublishingGroups($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing recipe categories... '));
    $this->importer->importRecipeCategories($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing recipe sources... '));
    $this->importer->importRecipeSources($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing recipes... '));
    $this->importer->importRecipes($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $this->output()->write(dt('Importing menus... '));
    $this->importer->importMenus($params);
    $this->output()->write(dt("Done!\r\n"));
    $this->printMemoryUsage();

    $end = time();
    $this->state->set('computrition_entity_last_import_run', $end);
    $this->lock->release('computrition_entity_import_all');
    $this->printElapsedTime($start, $end);
  }

  /**
   * Delete any entities that have been deleted on the Computrition end.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option bundle
   *   If provided, then only orphaned objects matching this bundle type will
   *   be purged.
   *
   * @command computrition_entity:purge-orphan-objects
   * @aliases cepoo
   */
  public function purgeOrphanObjects(array $options = ['bundle' => NULL]) {
    $this->output()->write(dt('Beginning orphaned computrition entity purge... '));
    $start = time();

    if (!$this->lock->acquire('computrition_entity_orphan_purge')) {
      $this->output()->write(dt("Error!\r\n"));
      $this->output()->writeln(dt('Unable to acquire lock. The purger is already running.'));
      return FALSE;
    }
    $this->output()->write(dt("Ok!\r\n"));

    if ($bundle = $options['bundle'] ?? NULL) {
      $this->purger->purgeOrphanObjects($bundle);
    }
    else {
      $this->purger->purgeOrphanObjects();
    }

    $this->lock->release('computrition_entity_orphan_purge');

    $end = time();
    $this->printMemoryUsage();
    $this->printElapsedTime($start, $end);
  }

  /**
   * Delete invalid Computrition object entities from the Drupal site.
   *
   * @command computrition_entity:delete-invalid
   * @aliases cedi
   */
  public function deleteInvalidObjects() {
    $this->output()->write(dt('Beginning deletion of invalid computrition entities... '));
    $start = time();

    if (!$this->lock->acquire('computrition_entity_delete_invalid')) {
      $this->output()->write(dt("Error!\r\n"));
      $this->output()->writeln(dt('Unable to acquire lock. The invalid entity remover is already running.'));
      return FALSE;
    }
    $this->output()->write(dt("Ok!\r\n"));

    $count = 0;

    // Delete any objects that are missing a required location reference.
    $types = ['meal', 'menu', 'menu_type', 'recipe'];
    $query = $this->entityTypeManager->getStorage('computrition_object')->getQuery();
    $ids = $query->condition('type', $types, 'IN')
      ->notExists('location.entity.id')
      ->accessCheck(FALSE)
      ->execute();
    foreach ($ids as $id) {
      $object = $this->entityTypeManager->getStorage('computrition_object')->load($id);
      $t_args = [
        ':title' => $object->get('name')->getValue()[0]['value'] ?? '',
        ':type' => $object->bundle() ?? '',
        ':id' => $object->id(),
      ];
      $this->output()->writeln(dt('Deleting: (:type) :title [id: :id]', $t_args));
      $object->delete();
      $count++;
    }

    // Delete any objects that are missing a required meal reference.
    $types = ['menu'];
    $query = $this->entityTypeManager->getStorage('computrition_object')->getQuery();
    $ids = $query->condition('type', $types, 'IN')
      ->notExists('meal.entity.id')
      ->accessCheck(FALSE)
      ->execute();
    foreach ($ids as $id) {
      $object = $this->entityTypeManager->getStorage('computrition_object')->load($id);
      $t_args = [
        ':title' => $object->get('name')->getValue()[0]['value'] ?? '',
        ':type' => $object->bundle() ?? '',
        ':id' => $object->id(),
      ];
      $this->output()->writeln(dt('Deleting: (:type) :title [id: :id]', $t_args));
      $object->delete();
      $count++;
    }

    // Delete any objects that are missing a required menu_type reference.
    $types = ['menu'];
    $query = $this->entityTypeManager->getStorage('computrition_object')->getQuery();
    $ids = $query->condition('type', $types, 'IN')
      ->notExists('menu_type.entity.id')
      ->accessCheck(FALSE)
      ->execute();
    foreach ($ids as $id) {
      $object = $this->entityTypeManager->getStorage('computrition_object')->load($id);
      $t_args = [
        ':title' => $object->get('name')->getValue()[0]['value'] ?? '',
        ':type' => $object->bundle() ?? '',
        ':id' => $object->id(),
      ];
      $this->output()->writeln(dt('Deleting: (:type) :title [id: :id]', $t_args));
      $object->delete();
      $count++;
    }

    $t_args = [':c' => $count];
    $this->output()->writeln(dt(':c invalid entities deleted.', $t_args));
    $this->lock->release('computrition_entity_delete_invalid');

    $end = time();
    $this->printElapsedTime($start, $end);
  }

  /**
   * Output the amount of memory being used by the script to the console.
   */
  protected function printMemoryUsage() {
    $mem_usage = memory_get_usage(TRUE);
    $t_args = [
      ':mem' => round($mem_usage / 1048576, 2),
    ];
    $message = dt('*** Memory usage: :mem MB.', $t_args);
    $this->output()->writeln($message);
  }

  /**
   * Output the amount of time between a given starting and ending timestamp.
   *
   * @param int $start
   *   The starting time in unixtime form.
   * @param int $end
   *   The ending time in unixtime form.
   */
  protected function printElapsedTime(int $start, int $end) {
    $elapsed = $end - $start;
    $dtArgs = [
      ':h' => floor($elapsed / 3600),
      ':m' => floor(($elapsed / 60) % 60),
      ':s' => $elapsed % 60,
    ];
    if ($dtArgs[':h'] > 0) {
      $timeMessage = dt('Operation completed in :h hour(s), :m minute(s), and :s second(s).', $dtArgs);
    }
    elseif ($dtArgs[':m'] > 0) {
      $timeMessage = dt('Operation completed in :m minute(s) and :s second(s).', $dtArgs);
    }
    else {
      $timeMessage = dt('Operation completed in :s second(s).', $dtArgs);
    }

    $this->output()->write("\r\n");
    $this->output()->writeln($timeMessage);
  }

}
