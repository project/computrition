<?php

namespace Drupal\computrition_entity;

use Drupal\computrition\Api\ComputritionApiRequest;
use Drupal\computrition_entity\Entity\ComputritionObject;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\State\StateInterface;

/**
 * Imports data from the external Computrition API into Drupal entities.
 */
class ComputritionEntityImporter {

  /**
   * Computrition API request object used for fetching data from Computrition.
   *
   * @var \Drupal\computrition\Api\ComputritionApiRequest
   */
  protected $api;

  /**
   * Entity type manager used for creating our imported drupal entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Lock used to prevent multiple imports from running concurrently.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Logger factory used for writing to the Drupal database log.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Module handler used for making our import functions hookable.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The user-defined module configuration options.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * State object for tracking when the last import ran.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The database connection for making static queries.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * ComputritionEntityImporter constructor.
   *
   * @param \Drupal\computrition\Api\ComputritionApiRequest $api
   *   The computrition API request object used for pulling data from the
   *   external computrition API.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager used for creating our imported drupal entities.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   DatabaseLockBackend used to prevent multiple imports from running
   *   concurrently and trampling each other.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger factory used for writing messages to the Drupal database log.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   Module handler used for making our import functions hookable.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The user-defined module configuration options.
   * @param \Drupal\Core\State\StateInterface $state
   *   State object for tracking when the last import ran.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection for making static queries.
   */
  public function __construct(ComputritionApiRequest $api, EntityTypeManagerInterface $entity_type_manager, LockBackendInterface $lock, LoggerChannelFactory $logger, ModuleHandler $module_handler, ConfigFactoryInterface $config_factory, StateInterface $state, Connection $connection) {
    $this->api = $api;
    $this->entityTypeManager = $entity_type_manager;
    $this->lock = $lock;
    $this->logger = $logger->get('computrition_entity');
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('computrition.settings');
    $this->state = $state;
    $this->connection = $connection;
  }

  /**
   * Fetch all data from the computrition API and import into Drupal entities.
   *
   * @param array $options
   *   An array of optional settings to pass into each individual importer
   *   function.
   */
  public function importAll(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_all')) {
      $this->logger->notice('Unable to run full computrition entity import as it is already running.');
      return FALSE;
    }

    $this->importLocations($options);
    $this->importMeals($options);
    $this->importMenuTypes($options);
    $this->importNutrients($options);
    $this->importPublishingGroups($options);
    $this->importRecipeCategories($options);
    $this->importRecipeSources($options);
    $this->importRecipes($options);
    $this->importMenus($options);

    $this->state->set('computrition_entity_last_import_run', time());

    $this->lock->release('computrition_entity_import_all');
    $this->logger->notice('Computrition full entity import run completed.');

    return TRUE;
  }

  /**
   * Import all computrition location objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importLocations(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_locations')) {
      $this->logger->notice('Unable to import computrition locations, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $items = $this->api->getLocationList();

    foreach ($items as $item) {
      if (!empty($item)) {
        $this->importLocation($item, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_locations');
  }

  /**
   * Helper function to import a single location into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importLocation(array $item, bool $skipHashCheck) {
    $changed = FALSE;
    $locationName = $item['description'] ? $item['description'] : $item['sid'];

    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'location',
      'remote_id' => $item['sid'],
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $locationName);
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'location',
        'name' => $locationName,
        'remote_id' => $item['sid'],
        'remote_data' => $item,
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported location.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_location_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition meal objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importMeals(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_meals')) {
      $this->logger->notice('Unable to import computrition meals, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;

    // We need to individually fetch the meals for each location in the
    // system.
    $properties = [
      'type' => 'location',
    ];
    $locations = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    foreach ($locations as $location) {
      if (!$this->locationIsAllowed($location)) {
        continue;
      }

      $items = $this->api->getMealList(['sid' => $location->getRemoteId()]);
      foreach ($items as $item) {
        if (!empty($item)) {
          $this->importMeal($item, $location, $skipHashCheck);
        }
      }
    }

    $this->lock->release('computrition_entity_import_meals');
  }

  /**
   * Helper function to import a single meal into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importMeal(array $item, ComputritionObject $location, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'meal',
      'remote_id' => $item['id'],
      'location' => $location->id(),
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('location', $location->id());
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'meal',
        'name' => $item['name'],
        'remote_id' => $item['id'],
        'remote_data' => $item,
        'location' => $location->id(),
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported meal.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_meal_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition menu type objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importMenuTypes(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_menutypes')) {
      $this->logger->notice('Unable to import computrition menu types, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;

    // We need to individually fetch the menu types for each location in the
    // system.
    $properties = [
      'type' => 'location',
    ];
    $locations = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    foreach ($locations as $location) {
      if (!$this->locationIsAllowed($location)) {
        continue;
      }

      $items = $this->api->getMenuTypeList(['sid' => $location->getRemoteId()]);
      foreach ($items as $item) {
        if (!empty($item)) {
          $this->importMenuType($item, $location, $skipHashCheck);
        }
      }
    }

    $this->lock->release('computrition_entity_import_menutypes');
  }

  /**
   * Helper function to import a single menu type into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importMenuType(array $item, ComputritionObject $location, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'menu_type',
      'remote_id' => $item['id'],
      'location' => $location->id(),
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('location', $location->id());
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'menu_type',
        'name' => $item['name'],
        'remote_id' => $item['id'],
        'remote_data' => $item,
        'location' => $location->id(),
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported menu type.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_menu_type_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition nutrient objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importNutrients(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_nutrients')) {
      $this->logger->notice('Unable to import computrition nutrients, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $items = $this->api->getNutrientList();

    foreach ($items as $item) {
      if (!empty($item)) {
        $this->importNutrient($item, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_nutrients');
  }

  /**
   * Helper function to import a single nutrient into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importNutrient(array $item, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'nutrient',
      'remote_id' => $item['interfacecode'],
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'nutrient',
        'name' => $item['name'],
        'remote_id' => $item['interfacecode'],
        'remote_data' => $item,
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported nutrient.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_nutrient_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition publishing group objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importPublishingGroups(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_pubgroups')) {
      $this->logger->notice('Unable to import computrition publishing groups, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $items = $this->api->getPublishingGroupList();

    foreach ($items as $item) {
      if (!empty($item)) {
        $this->importPublishingGroup($item, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_pubgroups');
  }

  /**
   * Helper function to import a single publishing group into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importPublishingGroup(array $item, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'publishing_group',
      'remote_id' => $item['id'],
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'publishing_group',
        'name' => $item['name'],
        'remote_id' => $item['id'],
        'remote_data' => $item,
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported publishing group.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_publishing_group_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition recipe category objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importRecipeCategories(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_recipecats')) {
      $this->logger->notice('Unable to import computrition recipe categories, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $items = $this->api->getRecipeCategoryList();

    foreach ($items as $item) {
      if (!empty($item)) {
        $this->importRecipeCategory($item, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_recipecats');
  }

  /**
   * Helper function to import a single recipe category into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importRecipeCategory(array $item, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'recipe_category',
      'remote_id' => $item['id'],
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'recipe_category',
        'name' => $item['name'],
        'remote_id' => $item['id'],
        'remote_data' => $item,
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported recipe category.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_recipe_category_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition recipe source objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importRecipeSources(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_recipesrc')) {
      $this->logger->notice('Unable to import computrition recipe sources, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $items = $this->api->getRecipeSourceList();

    foreach ($items as $item) {
      if (!empty($item)) {
        $this->importRecipeSource($item, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_recipesrc');
  }

  /**
   * Helper function to import a single recipe source into Drupal.
   *
   * @param array $item
   *   An array representing a computrition object as resturned from a
   *   computrition API response.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importRecipeSource(array $item, bool $skipHashCheck) {
    $changed = FALSE;
    // If an entity already exists with the same remote_id, we update it.
    // Otherwise, we create a new one.
    $properties = [
      'type' => 'recipe_source',
      'remote_id' => $item['id'],
    ];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$item]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $item['name']);
        $object->set('remote_data', $item);
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'recipe_source',
        'name' => $item['name'],
        'remote_id' => $item['id'],
        'remote_data' => $item,
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported recipe source.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_recipe_source_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition recipe objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importRecipes(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_recipes')) {
      $this->logger->notice('Unable to import computrition recipes, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;

    // We need to individually fetch the recipes for each location in the
    // system.
    $properties = [
      'type' => 'location',
    ];
    $locations = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    foreach ($locations as $location) {
      if (!$this->locationIsAllowed($location)) {
        continue;
      }

      $items = $this->api->getRecipeList(['sid' => $location->getRemoteId()]);

      if ($this->config->get('verbose_logging')) {
        $subs = ['@loc' => $location->getRemoteId()];
        $this->logger->debug('Importing recipes for location @loc', $subs);
        $subs = ['@c' => 0, '@total' => count($items)];
      }

      foreach ($items as $item) {
        if (!empty($item)) {
          if ($this->config->get('verbose_logging')) {
            $this->logger->debug('Importing recipe #@c of @total', $subs);
            $subs['@c']++;
          }
          $this->importRecipe($item, $location, $skipHashCheck);
        }
      }

      $this->entityTypeManager->getStorage('computrition_object')->resetCache([$location->id()]);
    }

    $this->lock->release('computrition_entity_import_recipes');
  }

  /**
   * Helper function to import a single recipe into Drupal.
   *
   * @param array $recipe
   *   An array representing a computrition object as returned from a
   *   computrition API response.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importRecipe(array $recipe, ComputritionObject $location, bool $skipHashCheck) {
    $changed = FALSE;

    // This recipe may already have an existing imported entity in the system.
    // If the user has elected to skip updating recipes that dont appear in
    // menus, we do that here.
    $onlyUpdateRecipesInMenus = $this->config->get('only_update_recipes_in_menus');
    if ($onlyUpdateRecipesInMenus) {
      $recipesInMenus = $this->getRecipeIdsInMenus();
      if (!in_array($recipe['id'], $recipesInMenus) && $this->recipeEntityExists($recipe['id'], $location)) {
        return;
      }
    }

    // If an entity already exists with the same remote_id and location,
    // we update it. Otherwise, we create a new one.
    $properties = [
      'type' => 'recipe',
      'remote_id' => $recipe['id'],
      'location' => $location->id(),
    ];
    // We could load the complete data for this recipe with a follow-up API
    // call, but would make the import make a computrition call for every
    // recipe in the system - very expensive and slow. Perhaps this should
    // be made an option?
    //
    // Just need to add: `$recipe = $this->api->getRecipe($recipe['id']);`.
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$recipe]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $recipe['name']);
        $object->set('remote_data', $recipe);
        $object->set('location', $location->id());
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'recipe',
        'name' => $recipe['name'],
        'remote_id' => $recipe['id'],
        'remote_data' => $recipe,
        'location' => $location->id(),
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported recipe.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_recipe_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')->resetCache([$object->id()]);
  }

  /**
   * Import all computrition menu objects into drupal entities.
   *
   * @param array $options
   *   An array of optional settings to change the import behavior. Available
   *   values are:
   *     - 'skip_hash_check': If TRUE, then existing entities will be
   *       force-reimported even if they haven't been modified.
   */
  public function importMenus(array $options = []) {
    if (!$this->lock->acquire('computrition_entity_import_menus')) {
      $this->logger->notice('Unable to import computrition menus, it is already running.');
      return FALSE;
    }

    $skipHashCheck = $options['skip_hash_check'] ?? FALSE;
    $numberOfDays = $this->config->get('menu_import_days');

    // We need to individually fetch the daily menus for each location in the
    // system.
    $properties = [
      'type' => 'location',
    ];
    $locations = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    for ($i = 0; $i <= $numberOfDays; $i++) {
      $time = strtotime("now +$i days");
      $date = date('Ymd', $time);
      foreach ($locations as $location) {
        if (!$this->locationIsAllowed($location)) {
          continue;
        }

        $menuParams = [
          'sid' => $location->getRemoteId(),
        ];
        $this->importMenuDay($date, $location, $menuParams, $skipHashCheck);
      }
    }

    $this->lock->release('computrition_entity_import_menus');
  }

  /**
   * Helper function to import a single day's worth of menus.
   *
   * @param string $date
   *   A date in the format "Ymd".
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param array $menuParams
   *   An array of parameters for fetching a menu from the computrition API.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importMenuDay(string $date, ComputritionObject $location, array $menuParams, bool $skipHashCheck) {
    if ($this->config->get('verbose_logging')) {
      $this->logger->debug('Importing day for ' . $location->get('name')->getValue()[0]['value'] . ': ' . $date);
    }
    $menuList = $this->api->getMenuList($date, $menuParams);

    $count = 0;
    foreach ($menuList as $menu) {
      if (!empty($menu)) {
        if ($this->config->get('verbose_logging')) {
          $this->logger->debug('-- Importing menu: ' . $menu['name'] . ' (' . $menu['id'] . ')');
        }
        $this->importMenu($menu, $menuParams, $location, $skipHashCheck);
        $count++;
      }
    }
  }

  /**
   * Helper function to import a single menu into Drupal.
   *
   * @param array $menu
   *   An array representing a computrition menu as returned from a
   *   computrition API response.
   * @param array $menuParams
   *   An array of parameters for fetching a menu from the computrition API.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param bool $skipHashCheck
   *   If TRUE, update existing computrition entities in Drupal even they
   *   haven't actually changed from the last import.
   */
  protected function importMenu(array $menu, array $menuParams, ComputritionObject $location, bool $skipHashCheck) {
    $changed = FALSE;

    // We do a follow up request to get the complete menu data.
    $menu = $this->api->getMenu($menu['id'], $menuParams);
    if (empty($menu)) {
      // The menu list returned a menu, but trying to look up the menu
      // individually failed.
      $subs = ['@id' => $menu['id'] ?? ''];
      $this->logger->warning('Failed to import menu [remote_id: @id]: individual menu lookup failed.', $subs);
      return;
    }

    // We ensure that we dont have configuration blocking the import of this
    // specific type of menu.
    if (!$this->menuTypeIsAllowed($location, $menu)) {
      return;
    }

    // Given a menu type ID, get the existing menu type entity for it.
    $properties = [
      'type' => 'menu_type',
      'location' => $location->id(),
      'remote_id' => $menu['menu']['menuid'],
    ];
    $menuTypes = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    $menuType = array_pop($menuTypes);
    unset($menuTypes);

    // Given a meal remote ID, get the existing meal entity for it.
    $properties = [
      'type' => 'meal',
      'location' => $location->id(),
      'remote_id' => $menu['menu']['mealid'],
    ];
    $meals = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    $meal = array_pop($meals);
    unset($meals);

    if ($meal === NULL) {
      $subs = ['@id' => $menu['id'] ?? ''];
      $this->logger->warning('Failed to import menu [remote_id: @id]: the provided meal period was invalid', $subs);
      return;
    }

    // For all of the recipes in this menu, build an array of entity id's
    // to reference each one in the menu object.
    $recipeIds = $this->getMenuRecipeIds($menu, $location);

    $date = \DateTime::createFromFormat('Ymd', $menu['menu']['servedate']);
    $properties = [
      'type' => 'menu',
      'remote_id' => $menu['menu']['id'],
      'serve_date' => $date->format('Y-m-d'),
      'meal' => $meal->id(),
    ];
    $menu_name = $menu['menu']['name'] . ' - ' . $menu['menu']['mealname'] . ' - ' . $properties['serve_date'];
    $existing = $this->entityTypeManager->getStorage('computrition_object')->loadByProperties($properties);
    if (!empty($existing)) {
      $object = array_pop($existing);
      $newHash = hash('crc32', Json::encode([$menu]));
      $oldHash = hash('crc32', Json::encode($object->getRemoteData()));
      if ($skipHashCheck || ($newHash != $oldHash)) {
        $object->set('name', $menu_name);
        $object->set('remote_data', $menu);
        $object->set('location', $location->id());
        $object->set('serve_date', $date->format('Y-m-d'));
        $object->set('meal', $meal->id());
        $object->set('menu_items', $recipeIds);
        $object->set('menu_type', $menuType->id());
        $changed = TRUE;
      }
    }
    else {
      $values = [
        'type' => 'menu',
        'name' => $menu_name,
        'remote_id' => $menu['menu']['id'],
        'remote_data' => $menu,
        'location' => $location->id(),
        'serve_date' => $date->format('Y-m-d'),
        'meal' => $meal->id(),
        'menu_items' => $recipeIds,
        'menu_type' => $menuType->id(),
      ];

      /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $object */
      $object = $this->entityTypeManager->getStorage('computrition_object')->create($values);
      $changed = TRUE;
    }

    // Allow other modules to hook in and modify the imported menu.
    $preAlterHash = hash('crc32', Json::encode($object->toArray()));
    $this->moduleHandler->alter('computrition_menu_import', $object);
    $postAlterHash = hash('crc32', Json::encode($object->toArray()));
    $changed = ($skipHashCheck || $changed || ($preAlterHash != $postAlterHash));
    if ($changed) {
      $object->save();
    }

    $this->entityTypeManager->getStorage('computrition_object')
      ->resetCache([$object->id()]);
  }

  /**
   * Helper function to ensure only content of desired locations are imported.
   *
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   *
   * @return bool
   *   TRUE if the menu type is allowed to be imported, FALSE otherwise.
   */
  protected function locationIsAllowed(ComputritionObject $location) {
    $allowedLocations = $this->config->get('allowed_locations');
    foreach ($allowedLocations as $index => $value) {
      if (!$value) {
        unset($allowedLocations[$index]);
      }
    }

    // No types have been configured, so all locations are allowed.
    if (empty($allowedLocations)) {
      return TRUE;
    }

    $locationId = $location->getRemoteId();
    $is_allowed = (isset($allowedLocations[$locationId]) && ($allowedLocations[$locationId]));

    return $is_allowed;
  }

  /**
   * Helper function to ensure only menus of a desired type are imported.
   *
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   * @param array $menu
   *   An array representing a computrition menu as returned from a
   *   computrition API response.
   *
   * @return bool
   *   TRUE if the menu type is allowed to be imported, FALSE otherwise.
   */
  protected function menuTypeIsAllowed(ComputritionObject $location, array $menu) {
    $allowedMenuTypes = $this->config->get('allowed_menu_types');
    foreach ($allowedMenuTypes as $index => $value) {
      if (!$value) {
        unset($allowedMenuTypes[$index]);
      }
    }

    // No types have been configured, so all menus are allowed.
    if (empty($allowedMenuTypes)) {
      return TRUE;
    }

    $menuId = $menu['menu']['menuid'];
    $locationId = $location->getRemoteId();
    $key = $locationId . '_' . $menuId;

    $is_allowed = (isset($allowedMenuTypes[$key]) && ($allowedMenuTypes[$key]));

    return $is_allowed;
  }

  /**
   * Helper function to get all of the recipe IDs in a given menu.
   *
   * @param array $menu
   *   An array representing a computrition menu as resturned from a
   *   computrition API response.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   *
   * @return array
   *   An array of recipe entity IDs.
   */
  protected function getMenuRecipeIds(array $menu, ComputritionObject $location) {
    $recipeIds = [];
    foreach ($menu['recipes'] as $recipe) {
      // When returned from the menu API call, the recipe ID is
      // concatenated with its menu rank. We undo that here to find our
      // stored recipe entity.
      if (!isset($recipe['id'])) {
        continue;
      }

      $idArray = explode('-', $recipe['id']);
      $query = $this->entityTypeManager->getStorage('computrition_object')->getQuery();
      $ids = $query->condition('type', 'recipe')
        ->condition('remote_id', $idArray[0])
        ->condition('location.target_id', $location->id())
        ->accessCheck(FALSE)
        ->execute();
      if ($id = array_pop($ids)) {
        $recipeIds[] = $id;
      }
      else {
        $subs = [
          '@rid' => $idArray[0],
          '@lid' => $location->id(),
        ];
        $this->logger->warning('Unable to find existing recipe entity for remote id @rid and location @lid.', $subs);
      }
    }

    return $recipeIds;
  }

  /**
   * Return an array of recipe remote IDs that only appear in menus.
   *
   * @return array
   *   An array of recipe remote IDs.
   */
  protected function getRecipeIdsInMenus() {
    static $data = [];

    if (isset($data) && !empty($data)) {
      return $data;
    }
    else {
      // We use a static query for best performance. Get the remote_id of all
      // recipes that appear in a menu.
      $queryString = "
        SELECT co_recipe.remote_id
        FROM {computrition_object} co_recipe
        INNER JOIN {computrition_object__menu_items} co_mi ON co_mi.menu_items_target_id = co_recipe.id
        INNER JOIN {computrition_object} co_menu ON co_menu.id = co_mi.entity_id
        WHERE co_recipe.type = 'recipe'
          AND co_menu.type = 'menu'
        GROUP BY co_recipe.remote_id
      ";
      $return = $this->connection->query($queryString)->fetchAll();
      foreach ($return as $item) {
        $data[] = $item->remote_id;
      }
    }

    return $data;
  }

  /**
   * Check if an imported entity exists for a recipe remote ID.
   *
   * @param string $recipeRemoteId
   *   The remote ID of a recipe in Computrition.
   * @param \Drupal\computrition_entity\Entity\ComputritionObject $location
   *   A computrition object entity representing a dining location.
   *
   * @return bool
   *   TRUE if an entity exists for this recipe, FALSE otherwise.
   */
  protected function recipeEntityExists(string $recipeRemoteId, ComputritionObject $location) {
    $params = [
      ':recipeId' => $recipeRemoteId,
      ':locId' => $location->id(),
    ];

    // We use a static query for best performance. Get any entity id's
    // corresponding to the given recipe remote id and location entity id.
    $queryString = "
      SELECT co_recipe.id
      FROM {computrition_object} co_recipe
      INNER JOIN {computrition_object__location} co_l ON co_l.entity_id = co_recipe.id
      WHERE co_recipe.type = 'recipe'
        AND co_recipe.remote_id = :recipeId
        AND co_l.location_target_id = :locId
    ";
    $result = $this->connection->query($queryString, $params)->fetchAll();

    return !empty($result);
  }

}
