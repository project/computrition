<?php

namespace Drupal\computrition_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Computrition object entity.
 *
 * @see \Drupal\computrition_entity\Entity\ComputritionObject.
 */
class ComputritionObjectAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\computrition_entity\Entity\ComputritionObjectInterface $entity */

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view computrition object entities');

      case 'edit':
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit computrition object entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete computrition object entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add computrition object entities');
  }

}
